open Batteries
open Common
open Execution
open Bench_types 


module Exec(A: Arch.Arch_t) = struct
  include Bench(A)
  module Tile = Codegen.Tile(A)
  module Pred = Arch.Pred_conv(A)
  open Conv_spec

  let dim_sizes tile =
    let open Tile.Conv in
    let ds = dim_tile_size tile in
    ds f_dim, ds c_dim, ds y_dim, ds x_dim, ds h_dim, ds w_dim

  let build_args (f,c,y,x,h,w) stride =
    match stride with
      None -> Config.conv_args x w y h c f
    | Some (sx, sy) ->
      Config.conv_args_stride x w y h c f sx sy

  let exec_scheme_size_stride counters_list size stride scheme  =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:10
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
    let args = build_args size stride in
    let res = exec conf_gen args (Tile.Conv_tile scheme) in
    List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
      counters_list

  let exec_scheme counters_list {adapted_size; stride;_} scheme =
    exec_scheme_size_stride counters_list adapted_size stride scheme

  let exec_print_counters_size_stride counters_list ((f,c,y,x,h,w) as size) stride  scheme =
    let open Counter in
    List.iter (fun (counter, n) -> match counter with
        | CYCLES ->
          let p = Pred.peak_percent f c y x h w n in
          Printf.printf "%d cycles, peak perf : %.2f%%\n" n p
        | _ -> Printf.printf "%s : %d\n" (show counter) n)
      (exec_scheme_size_stride counters_list size stride scheme)

  let exec_print_counters counters_list {adapted_size ; stride;_}  scheme =
    exec_print_counters_size_stride counters_list adapted_size stride scheme

  let gen_file_size_stride postfix size stride scheme =
    let args = build_args size stride in
    let open Config in
    gen_file (Conv_layout (XYF, XYC, HWCF))
      args (Tile.Conv_tile scheme) postfix

  let gen_file postfix {adapted_size; stride;_} scheme =
    gen_file_size_stride postfix adapted_size stride scheme
end

(* Example with MobileNet1 *)
let () =
  let open Conv_sizes in
  let open Exec(Arch.Haswell) in
  let mb_scheme = Tile.Conv.[V f_dim; U (4, f_dim); U (7, y_dim); U (3, h_dim);
                             T (32, c_dim); Hoist_vars [c_dim]; T (8, x_dim);
                             T (3, w_dim); T (2, c_dim); T (1, f_dim); T (8, y_dim);
                             T (7, x_dim); T (2, f_dim); T (1, f_dim)] in
  exec_print_counters Counter.[CYCLES; L1I_MISS] mobilNet_2_spec mb_scheme

(* Example with our best microkernel *)
let () =
  let open Exec(Arch.Haswell) in
  let microkern = Tile.Conv.[V f_dim; U (2, f_dim); U (5, y_dim);
                             U (3, h_dim); T (256, c_dim);
                             Hoist_vars [c_dim]] in
  exec_print_counters_size_stride Counter.[CYCLES; L1I_MISS]
    (dim_sizes microkern) None microkern

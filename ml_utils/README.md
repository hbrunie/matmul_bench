* Run binary

binaries are described in dune file and can be launched with :
dune exec ./{name of the executable}

For example, ics_search is declared in dune as :

    (executable
     (name ics_search)
     (modes native)
     (modules ics_search)
     (libraries execution common search batteries streaming tensor_loops ppx_deriving)
     (preprocess
      (pps ppx_deriving.show  ppx_deriving.eq ppx_deriving.ord )
     )
    )

and is called as :
dune exec ./ics_search.exe

the relevant file is ics_search/ics_search.ml. Dune sees the file hierarchy as if it were flat
thanks to the stanza (include\_subdirs unqualified) (first line).

For convenience a Makefile is provided. It can be used to launch the executable with :
make ics\_search for example.

Executables are :  ics_search scratch stephane_search ics_eval microkern

* Binaries

** ics_eval

Evaluating the final implementations found for ics and compares against onednn

** ics_search

ics_search reproduce the search of candidates we used for ICS submission.
Generate all candidates based on ad-hoc permutation (one per bench) that are
then sorted by so-called "cache-occupation" - supposed to measure how good a scheme
fit at a given level of the loop nest.

** scratch

Minimal binary executing only one bench, sometimes useful as a basis for quick
scripting

** stephane_search

Close to ics_search, but taylored for stephane's need for micro_experience

** micro_search

Yet another candidates search, here done randomly. This evaluates a sample
of the whole space.

** microkern

Microkernel research. Evaluates the whole microkernel space, sorts by
performance and then dump into stdout

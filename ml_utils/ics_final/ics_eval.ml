open Batteries
open Common
open Utils
open Execution
open Bench_types
(* Streaming is a library for manipulating stream, sink or sources (in a
 * push-based or pull-based way.
 * It allows to decouple data "production" and data consuming. For example
 * you can use two different functions to execute many different
 * configuration, and in another function defines what you want to do with
 * them - print on stdout, writing to a file, computing a maximum, a
 * median... *)
open Streaming

(* This module is parametrized over Architecture. It contains a whole bunch of
 * experiments. This is not stable and is not intended to be. Any function
 * considered worth being stabilized should be moved to another file - maybe
 * data_process. *)
module Expe(A:Arch.Arch_t) = struct
  open Execution
  module Tile = Codegen.Tile(A)
  module MM = Tile.MM
  module Conv = Tile.Conv
  include Bench(A)
  include Config
  module Pred_mm = Arch.Pred_mm(A)
  module Pred_conv = Arch.Pred_conv(A)
end

let protect f filename =
  let fn = if Sys.file_exists filename then
     filename ^ "x"
    else filename in
  f fn

(* ICS *)
open Best_convs_fifty
open Conv_sizes

let ics (module A :Arch.Arch_t) output_name  =
  let open Expe(A) in
  let open Best(A) in
  let file_write =
    let init () = let f = File.open_out output_name in
      IO.write_line f "name, onednn, gen"; f
    and push f (name, onednn, gen) =
      Printf.fprintf f "%s, %f, %f\n" name onednn gen;
      IO.flush f;
      f
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop () in
  let print_advance  =
    let init () = ()
    and push () (name, onednn, gen) =
      Printf.printf "%s, %f, %f\n" name onednn gen;
      IO.flush_all ()
        and stop () = ()  in
    Sink.make ~init ~push ~stop () in
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES] in
  let conf_dnn = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (false, true, Gcc,  O3))
      [OneDNN; ] [CYCLES] in
  let name_sizes_scheme_strides = map4 (fun x y z s -> x, y , z, s)
      all_names all_sizes all_convs all_strides in
     Stream.of_list name_sizes_scheme_strides
    |>  Stream.map (fun (name, (f,c,y,x,h,w), tile, stride) ->
      let args = match stride with
        None -> conv_args x w y h c f
        | Some (sx, sy) -> conv_args_stride x w y h c f sx sy in
      let gen_score = exec conf_gen args (Tile.Conv_tile tile)
                         |> Config.conv_cycles
                         |> Pred_conv.peak_percent x y w h c f in
      let onednn_score = exec conf_dnn args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred_conv.peak_percent x y w h c f in
      (name, onednn_score, gen_score))
    |> Stream.into
    (Sink.zip_left file_write  print_advance )

let onednn (module A: Arch.Arch_t) =
  let open Expe(A) in
  let open Best(A) in
  let print_advance  =
    let init () = ()
    and push () (name, onednnflast, onednnxlast) =
      Printf.printf "%s, %.2f, %.2f\n" name onednnflast onednnxlast;
      IO.flush_all ()
        and stop () = ()  in
    Sink.make ~init ~push ~stop () in
  let conf_dnn_flast = build_conv_config_with_layout ~error:(Config.EPSILON 0.01) ~num_rep:20
      (Some (false, true, Gcc,  O3))
      [OneDNN, Conv_layout (XYF, XYC, WHCF) ; ] [CYCLES] in
  let conf_dnn_xlast = build_conv_config_with_layout ~error:(Config.EPSILON 0.01) ~num_rep:20
      (Some (false, true, Gcc,  O3))
      [OneDNN, Conv_layout (FYX, CYX, FCHW) ; ] [CYCLES] in
  let name_sizes_scheme_strides = map4 (fun x y z s -> x, y , z, s) all_names all_sizes
      all_convs all_strides in
  Stream.of_list name_sizes_scheme_strides
  |>  Stream.map (fun (name, (f,c,y,x,h,w), _, stride) ->
      let args = match stride with
          None -> conv_args x w y h c f
        | Some (sx, sy) -> conv_args_stride x w y h c f sx sy in
      let onednn_score_flast = exec conf_dnn_flast args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred_conv.peak_percent x y w h c f 
     and onednn_score_xlast = exec conf_dnn_xlast args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred_conv.peak_percent x y w h c f in
      (name, onednn_score_flast, onednn_score_xlast))
  |> Stream.into print_advance

open Best_known_convs
let yolo_exhaustive (module A :Arch.Arch_t) output_name  =
  let open Expe(A) in
  let open Config in
  let module Conv_l =
    (val Conv.build_conv_module XYF XYC HWCF) in
  let open Best(A)(Conv_l) in
  let file_write =
    let init () = let f = File.open_out output_name in
      IO.write_line f "name, onednn, gen"; f
    and push f (name, onednn, gen) =
      Printf.fprintf f "%s, %f, %f\n" name onednn gen;
      IO.flush f;
      f
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop () in
  let print_advance  =
    let init () = ()
    and push () (name, onednn, gen) =
      Printf.printf "%s, %f, %f\n" name onednn gen;
      IO.flush_all ()
        and stop () = ()  in
    Sink.make ~init ~push ~stop () in
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES] in
  let conf_dnn = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (false, true, Gcc,  O3))
      [OneDNN; ] [CYCLES] in
(*
  List.iter (fun (name,_,_) -> print_endline name) name_sizes_scheme
*)
  let exhaustive = map4 (fun x y z s -> x, y , z, s) exhaustives_names exhaustive_sizes
      exhaustive exhaustive_strides in
(*     Stream.of_list name_sizes_scheme_strides *)
     Stream.of_list exhaustive
    |>  Stream.map (fun (name, (f,c,y,x,h,w), tile, stride) ->
      let args = match stride with
        None -> conv_args x w y h c f
        | Some (sx, sy) -> conv_args_stride x w y h c f sx sy in
      let gen_score = exec conf_gen args (Tile.Conv_tile tile)
                         |> Config.conv_cycles
                         |> Pred_conv.peak_percent x y w h c f in
      let onednn_score = exec conf_dnn args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred_conv.peak_percent x y w h c f in
      (name, onednn_score, gen_score))
    |> Stream.into
    (Sink.zip_left file_write  print_advance )

let () = onednn (module Arch.Haswell)
let fefev () = protect (ics (module Arch.Sky_lake)) "retry.res"
let blub () = protect (yolo_exhaustive (module Arch.Sky_lake)) "all_exhaustive.res"

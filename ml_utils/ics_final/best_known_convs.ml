open Common
open Execution

module Best(A: Arch.Arch_t)(Conv: Tensor_loops.Sign.Conv_args_t) = struct
  module Tile = Codegen.Tile(A)
  include Conv
  include Tile.Conv

  let yolo0 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
               T (3, c_dim); Hoist_vars [c_dim]; T (16, x_dim);
               T (3, w_dim); T (1, h_dim);
               Lambda_apply (y_dim, [((Iter 9), (Arg 9)); ((Iter 5), (Arg 11))]);
               T (2, y_dim); T (34, x_dim); T (2, y_dim); T (1, f_dim)]

  let yolo2 = [V f_dim; U (4, f_dim); U (4, y_dim); U (3, w_dim); T (16, c_dim);
               Hoist_vars [c_dim]; T (2, x_dim);
               T (1, w_dim); T (3, h_dim); T (34, y_dim); T (1, f_dim); T (2, c_dim);
               T (136, x_dim); T (2, y_dim);
               T (1, f_dim)]
(*
  let yolo2 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim);
               T (3, w_dim); T (1, h_dim); T (2, y_dim); T (4, f_dim); T (2, c_dim); T (136, x_dim);
               Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 5), (Arg 10))])
              ]
*)

  let yolo4 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
  T (3, w_dim); T (1, h_dim); T (2, y_dim); T (4, f_dim); T (2, c_dim); T (136, x_dim);
  Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 5), (Arg 10))])]

  let yolo5 = [V f_dim; U (4, f_dim); ULambda y_dim; T (128, c_dim); Hoist_vars [c_dim];
               T (2, x_dim); T (4, x_dim);
               Lambda_apply (y_dim, [((Iter 1), (Arg 6)); ((Iter 4), (Arg 7))]); T (1, f_dim);
               T (1, c_dim); T (17, x_dim); T (4, y_dim)]


  let yolo8 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
               T (32, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
               T (3, w_dim); T (1, h_dim); T (8, f_dim); T (1, x_dim);
               Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 5), (Arg 10))]); T (68, x_dim);
               T (1, y_dim); T (1, f_dim); T (4, c_dim)] 

  let yolo9 = [V f_dim; U (2, f_dim); ULambda y_dim; T (256, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim); T (4, x_dim);
               T (4, y_dim); T (4, f_dim); T (1, c_dim); T (17, x_dim);
               Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))])]

  let yolo12 = [V f_dim; U (2, f_dim); ULambda y_dim; T (32, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim); T (3, w_dim);
                T (3, h_dim); T (8, f_dim); T (1, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 2), (Arg 13))]);
                T (34, x_dim);
                T (2, f_dim); T (8, c_dim)]

  let yolo13 = [V f_dim; U (2, f_dim); ULambda y_dim; T (128, c_dim); Hoist_vars [c_dim];
                T (1, x_dim); T (1, x_dim); T (1, y_dim); T (8, f_dim); T (4, c_dim); T (34, x_dim);
                Lambda_apply (y_dim, [((Iter 2), (Arg 11)); ((Iter 1), (Arg 12))]);
                T (1, f_dim); T (1, c_dim); T (1, y_dim)]

  let yolo18 = [V f_dim; U (4, f_dim); ULambda y_dim; U (3, h_dim); T (16, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim);
                T (3, w_dim); T (1, h_dim); T (4, f_dim); T (1, c_dim); T (17, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 5)); ((Iter 2), (Arg 6))]); T (4, f_dim);
                T (32, c_dim)]

  let yolo19 = [V f_dim; U (2, f_dim); ULambda y_dim; T (64, c_dim); Hoist_vars [c_dim];
                T (1, x_dim); T (1, c_dim);
                T (16, f_dim); T (17, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]); T (16, c_dim)]

  let mobilNet_2 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim);
                   T (64, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                   T (3, w_dim); T (1, h_dim); T (8, x_dim);
                   Lambda_apply (y_dim, [((Iter 5), (Arg 12)); ((Iter 4), (Arg 13))]); T (4, f_dim);
                   T (1, c_dim); T (14, x_dim)]

  let mobilNet_3 = [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (64, c_dim);
                    Hoist_vars [c_dim]; T (1, x_dim);
                    T (3, w_dim); T (1, h_dim); T (4, y_dim); T (8, f_dim); T (2, c_dim);
                    T (56, x_dim); T (1, y_dim)]

  let mobilNet_5 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
                    T (32, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                    T (3, w_dim); T (1, h_dim); T (1, c_dim); T (8, f_dim); T (28, x_dim);
                    Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (8, c_dim)]

  let mobilNet_6 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
  T (3, w_dim); T (1, h_dim); T (1, c_dim); T (8, f_dim); T (28, x_dim);
  Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (8, c_dim)]

  let mobilNet_8 = [V f_dim; U (4, f_dim); ULambda y_dim; U (3, w_dim); T (16, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
  T (1, w_dim); T (3, h_dim); T (1, c_dim); T (8, f_dim); T (14, x_dim);
  Lambda_apply (y_dim, [((Iter 2), (Arg 4)); ((Iter 1), (Arg 6))]); T (32, c_dim)]

  let mobilNet_9 = [V f_dim; U (4, f_dim); U (7, y_dim); U (3, h_dim); T (16, c_dim);
                    Hoist_vars [c_dim]; T (7, x_dim);
                    T (3, w_dim); T (1, h_dim); T (1, c_dim); T (16, f_dim); T (16, c_dim);
                    T (4, c_dim)]

  let resNet4 = [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (64, c_dim);
                 Hoist_vars [c_dim]; T (1, x_dim); T (3, w_dim); T (1, h_dim); T (8, x_dim);
                 T (4, y_dim); T (8, f_dim); T (1, c_dim); T (7, x_dim)]

  let resNet7 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim);
                 Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (1, c_dim); T (8, f_dim); T (28, x_dim);
                 Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (4, c_dim)] 

  let resNet8 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim);
                 T (64, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (8, f_dim); T (1, c_dim); T (2, x_dim);
                 Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (14, x_dim);
                 T (2, f_dim); T (2, c_dim); T (1, x_dim)]

  let resNet10 = [V f_dim; U (4, f_dim); ULambda y_dim; U (3, w_dim);
                  T (16, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                  T (1, w_dim); T (3, h_dim); T (1, c_dim); T (8, f_dim); T (14, x_dim);
                  Lambda_apply (y_dim, [((Iter 1), (Arg 4)); ((Iter 2), (Arg 5))]); T (32, c_dim)]



  let exhaustive = [yolo0; yolo2; yolo4; yolo5; yolo8; yolo9; yolo12; yolo13; yolo18;
                          yolo19;
                          mobilNet_2; mobilNet_3; mobilNet_5; mobilNet_6; mobilNet_8;mobilNet_9; 
                          resNet4; resNet7; resNet8; resNet10
               (*yolo23 TODO *)]

end

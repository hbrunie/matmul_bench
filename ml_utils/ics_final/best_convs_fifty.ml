open Common
open Execution

module Best(A: Arch.Arch_t) = struct
  module Tile = Codegen.Tile(A)
  include Tile.Conv

  let yolo0 = [V f_dim; U (2, f_dim); ULambda y_dim; T (3, c_dim); Hoist_vars [c_dim];
               T (16, x_dim); T (3, w_dim); T (3, h_dim);
               Lambda_apply (y_dim, [((Iter 11), (Arg 11)); ((Iter 1), (Arg 15))]);
               T (2, y_dim); T (34, x_dim); T (2, y_dim); T (1, f_dim)]

  let yolo2 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim);
               Hoist_vars [c_dim]; T (2, x_dim); T (3, w_dim); T (1, h_dim);
               Lambda_apply (y_dim, [((Iter 9), (Arg 9)); ((Iter 5), (Arg 11))]); T (2, f_dim);
               T (1, c_dim); T (136, x_dim); T (2, y_dim); T (1, f_dim)]
(*
  let yolo2 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (32, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim);
               T (3, w_dim); T (1, h_dim); T (2, y_dim); T (4, f_dim); T (2, c_dim); T (136, x_dim);
               Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 5), (Arg 10))])
              ]
*)

  let yolo4 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim); T (64, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim);  T (3, w_dim); T (1, h_dim);
               T (2, y_dim); T (8, f_dim); T (1, c_dim); T (136, x_dim);
               Lambda_apply (y_dim, [((Iter 1), (Arg 12)); ((Iter 4), (Arg 14))])] 

  let yolo5 = [V f_dim; U (4, f_dim); ULambda y_dim; T (128, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim); T (34, x_dim);
               T (4, y_dim); T (1, f_dim); T (1, c_dim); T (4, x_dim);
               Lambda_apply (y_dim, [((Iter 1), (Arg 6)); ((Iter 4), (Arg 7))])]

  let yolo8 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim); T (64, c_dim);
               Hoist_vars [c_dim]; T (1, x_dim);
               T (3, w_dim); T (1, h_dim); T (16, f_dim); T (17, x_dim);
               Lambda_apply (y_dim, [((Iter 1), (Arg 10)); ((Iter 2), (Arg 12))]); T (4, x_dim);
               T (2, y_dim); T (1, f_dim); T (2, c_dim)]

  let yolo9  = [V f_dim; U (2, f_dim); ULambda y_dim; T (256, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim); T (1, x_dim);
                Lambda_apply (y_dim, [((Iter 3), (Arg 8)); ((Iter 4), (Arg 11))]);
                T (4, f_dim); T (1, c_dim); T (68, x_dim); T (1, y_dim)]

  let yolo12 = [V f_dim; U (2, f_dim); ULambda y_dim; T (32, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim); T (3, w_dim);
                T (3, h_dim); T (8, f_dim); T (1, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 2), (Arg 13))]);
                T (34, x_dim);
                T (2, f_dim); T (8, c_dim)]

  let yolo13 = [V f_dim; U (2, f_dim); ULambda y_dim; T (256, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim); T (1, x_dim);
                T (1, y_dim); T (8, f_dim); T (2, c_dim); T (34, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 10)); ((Iter 2), (Arg 12))]);
                T (1, f_dim); T (1, c_dim); T (1, y_dim)]

  let yolo18 = [V f_dim; U (4, f_dim); ULambda y_dim; U (3, h_dim); T (16, c_dim);
                Hoist_vars [c_dim]; T (1, x_dim);
                T (3, w_dim); T (1, h_dim); T (2, f_dim); T (1, c_dim); T (17, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 5)); ((Iter 2), (Arg 6))]); T (8, f_dim);
                T (32, c_dim)]

  let yolo19 = [V f_dim; U (2, f_dim); ULambda y_dim; T (512, c_dim); Hoist_vars [c_dim]; T (1, f_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]); T (1, c_dim);
                T (17, x_dim); T (1, c_dim); T (16, f_dim); T (1, x_dim); T (1, y_dim); T (2, c_dim)]

  let yolo23 = [V f_dim; U (3, f_dim); ULambda y_dim; T (1024, c_dim);
                Hoist_vars [c_dim]; T (1, f_dim); T (1, y_dim);
                T (1, c_dim); T (1, x_dim); T (1, f_dim); T (17, x_dim);
                Lambda_apply (y_dim, [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]); T (31, f_dim);
                T (1, c_dim); T (19, f_dim)]

  let yolos = [yolo0; yolo2; yolo4; yolo5; yolo8; yolo9; yolo12; yolo13; yolo18;
               yolo19; (*yolo23 TODO *)]

  let mobilNet_1 = [V f_dim; U (2, f_dim); ULambda y_dim; T (32, c_dim); Hoist_vars [c_dim];
                    T (1, x_dim); T (3, w_dim); T (3, h_dim); T (1, x_dim);
                    Lambda_apply (y_dim, [((Iter 3), (Arg 8)); ((Iter 8), (Arg 11))]); T (1, c_dim);
                    T (112, x_dim)]

  let mobilNet_2 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim); T (64, c_dim);
                    Hoist_vars [c_dim]; T (1, x_dim);
                    T (3, w_dim); T (1, h_dim); T (4, x_dim);
                    Lambda_apply (y_dim, [((Iter 5), (Arg 12)); ((Iter 4), (Arg 13))]); T (4, f_dim);
                    T (1, c_dim); T (28, x_dim)]

  let mobilNet_2_with_stride =
    [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim);
     T (64, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
     T (3, w_dim); T (1, h_dim); T (4, x_dim);
     Lambda_apply (y_dim, [((Iter 3), (Arg 10)); ((Iter 2), (Arg 13))]); T (4, f_dim);
     T (1, c_dim); T (14, x_dim)]

  let mobilNet_3 =
    [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
     T (3, w_dim); T (1, h_dim); T (1, y_dim); T (8, f_dim); T (1, c_dim); T (56, x_dim); T (4, y_dim)]

  let mobilNet_4 =
    [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
     T (3, w_dim); T (1, h_dim); T (1, y_dim); T (8, f_dim); T (1, c_dim); T (56, x_dim); T (4, y_dim)]

  let mobilNet_4_with_stride = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
                           T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                           T (3, w_dim); T (1, h_dim); T (1, y_dim); T (4, f_dim);
                           T (1, c_dim); T (28, x_dim);
                           Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))])]

  let mobilNet_5 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim); Hoist_vars [c_dim];
                    T (14, x_dim); T (3, w_dim);
                    T (3, h_dim); T (1, c_dim); T (8, f_dim); T (2, x_dim); T (2, y_dim); T (1, c_dim)]

  let mobilNet_6 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim);
                    Hoist_vars [c_dim]; T (14, x_dim); T (3, w_dim);
                    T (3, h_dim); T (1, c_dim); T (8, f_dim); T (2, x_dim); T (2, y_dim); T (1, c_dim)]

  let mobilNet_6_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (256, c_dim);
                           Hoist_vars [c_dim]; T (14, x_dim); T (3, w_dim);
                           T (3, h_dim); T (1, c_dim); T (4, f_dim);
                           T (1, x_dim); T (2, y_dim); T (1, c_dim)]

  let mobilNet_7 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim);
                    Hoist_vars [c_dim]; T (14, x_dim); T (3, w_dim);
                    T (3, h_dim); T (1, c_dim); T (16, f_dim); T (1, x_dim); T (1, y_dim); T (2, c_dim)]

  let mobilNet_8 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim);
                    Hoist_vars [c_dim]; T (14, x_dim); T (3, w_dim);
                    T (3, h_dim); T (2, c_dim); T (16, f_dim); T (1, x_dim); T (1, y_dim); T (1, c_dim)]

  let mobilNet_8_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (128, c_dim);
                          Hoist_vars [c_dim]; T (7, x_dim); T (3, w_dim);
                          T (3, h_dim); T (1, c_dim); T (8, f_dim); T (1, x_dim); T (1, y_dim);
                          T (4, c_dim)]

  let mobilNet_9 = [V f_dim; U (4, f_dim); U (7, y_dim); T (128, c_dim);
                    Hoist_vars [c_dim]; T (7, x_dim); T (3, w_dim);
                    T (3, h_dim); T (1, c_dim); T (16, f_dim); T (4, c_dim); T (2, c_dim)]

  let mobilNets_no_stride =
    [mobilNet_1; mobilNet_2; mobilNet_3;
     mobilNet_4; mobilNet_5; mobilNet_6;
     mobilNet_7; mobilNet_8; mobilNet_9]

  let mobilNets =
    [mobilNet_1; mobilNet_2_with_stride; mobilNet_3;
     mobilNet_4_with_stride; mobilNet_5; mobilNet_6_with_stride;
     mobilNet_7; mobilNet_8_with_stride; mobilNet_9]


  let resNet1 = [V f_dim; U (4, f_dim); U (7, y_dim); T (3, c_dim); Hoist_vars [c_dim];
                 T (1, x_dim); T (7, w_dim);
                 T (7, h_dim); T (16, x_dim); T (32, y_dim); T (1, f_dim); T (1, c_dim); T (14, x_dim)]

  let resNet1_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (3, c_dim);
                        Hoist_vars [c_dim]; T (1, x_dim); T (7, w_dim);
                        T (7, h_dim); T (8, x_dim); T (16, y_dim); T (1, f_dim);
                        T (1, c_dim); T (14, x_dim)]

  let resNet2 = [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (64, c_dim);
                 Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (14, x_dim); T (4, y_dim); T (4, f_dim); T (1, c_dim);
                 T (4, x_dim)]

  let resNet3 =[V f_dim; U (4, f_dim); ULambda y_dim; T (64, c_dim); Hoist_vars [c_dim];
                T (7, x_dim); T (4, x_dim);
                Lambda_apply (y_dim, [((Iter 7), (Arg 6)); ((Iter 2), (Arg 7))]); T (1, f_dim);
                T (2, x_dim); T (1, y_dim)]

  let resNet4_with_stride = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
                        T (64, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                        T (3, w_dim); T (1, h_dim); T (1, x_dim);
                        Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (4, f_dim);
                        T (1, c_dim); T (28, x_dim)]

  let resNet4 = [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (64, c_dim);
                 Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (8, x_dim);
                 T (4, y_dim); T (8, f_dim); T (1, c_dim); T (7, x_dim)]

  let resNet5 = [V f_dim; U (4, f_dim); ULambda y_dim; T (64, c_dim);
                 Hoist_vars [c_dim]; T (7, x_dim); T (4, x_dim);
                 T (1, y_dim); T (2, f_dim); T (2, x_dim);
                 Lambda_apply (y_dim, [((Iter 7), (Arg 6)); ((Iter 2), (Arg 7))])]

  let resNet5_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (64, c_dim); Hoist_vars [c_dim]; T (4, x_dim); T (1, x_dim);
  T (4, y_dim); T (2, f_dim); T (7, x_dim); T (1, y_dim)]

  let resNet6 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim);
                 T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (1, x_dim);
                 Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (4, f_dim);
                 T (1, c_dim); T (28, x_dim)]

  let resNet7 = [V f_dim; U (2, f_dim); ULambda y_dim; U (3, h_dim); T (128, c_dim);
                 Hoist_vars [c_dim]; T (14, x_dim);
                 T (3, w_dim); T (1, h_dim); T (1, c_dim); T (8, f_dim); T (2, x_dim);
                 Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (1, c_dim)]

  let resNet7_with_stride =
    [V f_dim; U (4, f_dim); ULambda y_dim; U (3, w_dim); T (64, c_dim);
     Hoist_vars [c_dim]; T (1, x_dim);
  T (1, w_dim); T (3, h_dim); T (1, c_dim); T (4, f_dim); T (14, x_dim);
  Lambda_apply (y_dim, [((Iter 2), (Arg 4)); ((Iter 1), (Arg 6))]); T (2, c_dim)]

  let resNet8 = [V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim); T (128, c_dim);
                 Hoist_vars [c_dim]; T (1, x_dim);
                 T (3, w_dim); T (1, h_dim); T (1, f_dim); T (1, c_dim); T (14, x_dim);
                 Lambda_apply (y_dim, [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]); T (2, x_dim);
                 T (16, f_dim); T (1, c_dim); T (1, x_dim)]

  let resNet9 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim); Hoist_vars [c_dim];
                 T (14, x_dim); T (3, w_dim);
                 T (3, h_dim); T (8, f_dim); T (1, c_dim); T (1, x_dim);
                 T (1, y_dim); T (1, f_dim); T (1, c_dim)]

  let resNet10 = [V f_dim; U (2, f_dim); U (14, y_dim); T (256, c_dim); Hoist_vars [c_dim];
                  T (14, x_dim); T (3, w_dim);
                  T (3, h_dim); T (1, c_dim); T (16, f_dim); T (1, x_dim); T (1, y_dim); T (2, c_dim)]

  let resNet10_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (256, c_dim);
                         Hoist_vars [c_dim]; T (7, x_dim); T (3, w_dim);
                         T (3, h_dim); T (1, c_dim); T (8, f_dim); T (1, x_dim); T (1, y_dim);
                         T (2, c_dim)]

  let resNet11 = [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); Hoist_vars [c_dim];
                  T (1, x_dim); T (1, x_dim);
                  T (16, f_dim); T (2, c_dim); T (14, x_dim); T (1, y_dim)]

  let resNet11_with_stride = [V f_dim; U (4, f_dim); U (7, y_dim); T (128, c_dim);
                         Hoist_vars [c_dim]; T (1, x_dim); T (1, x_dim);
                         T (8, f_dim); T (2, c_dim); T (7, x_dim); T (1, y_dim)]

  let resNet12 = [V f_dim; U (4, f_dim); U (7, y_dim); U (3, w_dim); T (32, c_dim);
                  Hoist_vars [c_dim]; T (7, x_dim); T (1, w_dim); T (3, h_dim);
                  T (1, c_dim); T (8, f_dim); T (16, c_dim)]


  let resNets_no_stride = [resNet1; resNet2; resNet3;
                 resNet4; resNet5; resNet6;
                 resNet7; resNet8; resNet9;
                 resNet10; resNet11; resNet12]

  let resNets = [resNet1_with_stride; resNet2; resNet3;
                 resNet4_with_stride; resNet5_with_stride; resNet6;
                 resNet7_with_stride; resNet8; resNet9;
                 resNet10_with_stride; resNet11_with_stride; resNet12]

  let all_convs = yolos @ mobilNets @ resNets
end

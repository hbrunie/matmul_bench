open Common
open Batteries
(* Bidule pour calculer footprint and co *)
type dim = F | C | Y | X | H | W [@@deriving eq, yojson, show {with_path = false}]

let python_of_dim = function
  | F -> Py.String.of_string "F"
  | C -> Py.String.of_string "C"
  | Y -> Py.String.of_string "Y"
  | X -> Py.String.of_string "X"
  | H -> Py.String.of_string "H"
  | W -> Py.String.of_string "W"

let dim_from_string = function
  "F" -> F | "C" -> C | "Y" -> Y | "X" -> X | "H" -> H | "W" -> W
  | _ -> failwith "Not a dimension"

let dim_of_python  =
   dim_from_string % Py.String.to_string

let enum_dim = [F; C; Y; X; H; W;] 

let kernel_dim_order = [F; Y; X; H; W; C] 

type kernel_spec =
  {varying_dim : dim;
   range : int * int;
   base_kern : (dim * int) list; 
   kern_order : dim list;
  } [@@deriving show {with_path = false}, eq, yojson]

(* Get a "representative" ukernel from a class *)
let kernel_repr sizes {varying_dim; range; base_kern;_} =
  let all_not_one = List.filter (fun (_, size) -> size <> 1) base_kern
  and median = let (min, max) = range in
    Stdlib.min (List.assoc varying_dim sizes) ((max + min) / 2) in
  (varying_dim, median) :: all_not_one


module type Arch_info_t = sig
  val l1_size: int
  val l2_size: int
  val l3_size: int
  val l1_bw: float
  val l2_bw: float
  val l3_bw: float
  val ukernels : kernel_spec list
  module A : Arch.Arch_t
end

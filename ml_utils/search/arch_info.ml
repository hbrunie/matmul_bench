open Common
open Conv_search_type
open Batteries
open Utils

type perfs = {x:int;y:int;h:int;w:int; c:int; f:int;res:float} [@@deriving show]

let to_perfs  = 
  function [Right x; Right y; Right w; Right h; Right c; Right f; Left res] ->
    {x;y;h;w;c; f ; res}
         | _ -> failwith "Invalid line"

let search_class vec_size threshold =
  let inj {x;h;w;c;f;_} = (x,h,w,c,f) in
  let cmp r1 r2 =
    [%ord:(int*int*int*int*int)] (inj r1) (inj r2) in
  List.filter (fun {res; _} -> res >= threshold)
  %> List.group cmp
  %> List.map (fun l ->
      let {x;h;w;f;_} = List.hd l in (* List is guaranteed not to be empty *)
      let min_y, max_y = List.min_max @@ List.map (fun {y;_} -> y) l in
      let base_kern = [F, f * vec_size; C, 1; X, x; H, h; W, w] in
      let select d s = if s > 1 then Some d else None in
      let kern_order =
        [Some F; Some Y; select H h; select W w]
        |> List.filter_map Fun.id in
      {varying_dim = Y; range = min_y, max_y; 
       base_kern;
       kern_order;
      }
    )

module Skylake_info : Arch_info_t = struct
  (* SIZE FOR MOTOKO *)
  let l1_size = 8192
  let l2_size = 262144
  let l3_size = 5046272

(* Old values, don't know where they come from
  let l1_bw = 8.
  let l2_bw = 8.
  let l3_bw = 0.75
*)

  (* This looks more reasonable ? *)
  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A =  Arch.Sky_lake

  let ukernels = [
    (* varying on Y *)
    (* Y = [8, 15]; F = 2 *)
    {varying_dim = Y; range = 8, 15;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [5, 10]; F = 3 *)
    {varying_dim = Y; range = 5, 10;
     base_kern = [(F, 48); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [6, 7]; F = 4 *)
    {varying_dim = Y; range = 6, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [5, 6]; F = 5 *)
    {varying_dim = Y; range = 5, 6;
     base_kern = [(F, 80); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [8, 15]; F = 1 / H = 3 *)
    {varying_dim = Y; range = 9, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [5, 12]; F = 2 / H = 3 *)
    {varying_dim = Y; range = 9, 12;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [5, 9]; F = 3 / H = 3 *)
    {varying_dim = Y; range = 5, 9;
     base_kern = [(F, 48); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [5, 7]; F = 4 / H = 3 *)
    {varying_dim = Y; range = 5, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [4, 5]; F = 5 / H = 3 *)
    {varying_dim = Y; range = 4, 5;
     base_kern = [(F, 80); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [4, 9]; F = 3  / W = 3 *)
    {varying_dim = Y; range = 4, 9;
     base_kern = [(F, 48); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    (* Y = [4, 7]; F = 4  / W = 3 *)
    {varying_dim = Y; range = 4, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    (* Y = [4, 5]; F = 5  / W = 3 *)
    {varying_dim = Y; range = 4, 5;
     base_kern = [(F, 80); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
  ]
end

module Silver_info : Arch_info_t = struct
  (* SIZE FOR MOTOKO *)
  let l1_size = 8192
  let l2_size = 262144
  let l3_size = 3670016

(* Old values, don't know where they come from
  let l1_bw = 8.
  let l2_bw = 8.
  let l3_bw = 0.75
*)

  (* This looks more reasonable ? *)
  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A =  Arch.Silver

  let ukernels = [

    (* W = 1 H = 1 *)
    (* Y = [7, 15]; F = 2  *)
    {varying_dim = Y; range = 7, 15;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y;];
    };
    (* Y = [7, 15]; F = 4  *)
    {varying_dim = Y; range = 3, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y;];
    };

    (* W = 1 H = 3 *)
    (* Y = [6, 15]; F = 2  *)
    {varying_dim = Y; range = 6, 15;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [7, 15]; F = 2  *)
    {varying_dim = Y; range = 6, 15;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [7, 15]; F = 4  *)
    {varying_dim = Y; range = 2, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };

    (* W = 3 H = 1 *)
    (* Y = [6, 15]; F = 2  *)
    {varying_dim = Y; range = 4, 8;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    {varying_dim = Y; range = 4, 13;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    {varying_dim = Y; range = 2, 7;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    {varying_dim = Y; range = 2, 3;
     base_kern = [(F, 128); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };


    (* W = 3 H = 3 *)
    (* Y = [4, 14]; F = 1 / H = 3 / W = 3 *)
    {varying_dim = Y; range = 4, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 3)];
     kern_order = [F; Y; H; W];
    };
    (* Y = [6, 15]; F = 2 / H = 3 / W = 3 *)
    {varying_dim = Y; range = 6, 15;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 3)];
     kern_order = [F; Y; H; W];
    };
  ]
end

module Skylake_avx2_info : Arch_info_t = struct
  (* SIZE FOR MOTOKO *)
  let l1_size = 8192
  let l2_size = 262144
  let l3_size = 5046272

  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A =  Arch.Haswell

  let ukernels = [
    (* varying on Y *)
    (* Y = [8, 14]; F = 2 *)
    {varying_dim = Y; range = 8, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (*
    (* Y = [5, 11]; F = 3 *)
    {varying_dim = Y; range = 5, 11;
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y];
      };
    *)
    (* Y = [5, 8]; F = 4 *)
    {varying_dim = Y; range = 5, 8;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (*
    (* Y = [3, 6]; F = 5 *)
    {varying_dim = Y; range = 3, 6;
      base_kern = [(F, 40); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y];
      };
      (* Y = [3, 5]; F = 5 *)
      {varying_dim = Y; range = 3, 5;
      base_kern = [(F, 48); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y];
      };
      (* Y = [3, 3]; F = 5 *)
      {varying_dim = Y; range = 3, 3;
      base_kern = [(F, 56); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y];
      };
    *)
    (* Y = [8, 15]; F = 2 / H = 3 *)
    {varying_dim = Y; range = 4, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (*
    (* Y = [3, 10]; F = 3 / H = 3 *)
    {varying_dim = Y; range = 3, 10;
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 3); (W, 1)];
      kern_order = [F; Y; H];
      };
    *)
    (* Y = [3, 8]; F = 4 / H = 3 *)
    {varying_dim = Y; range = 3, 8;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (*
    (* Y = [3, 6]; F = 5 / H = 3 *)
    {varying_dim = Y; range = 3, 6;
      base_kern = [(F, 40); (C, 1); (X, 1); (H, 3); (W, 1)];
      kern_order = [F; Y; H];
      };
      (* Y = [4, 5]; F = 5 / H = 3 *)
      {varying_dim = Y; range = 3, 6;
      base_kern = [(F, 48); (C, 1); (X, 1); (H, 3); (W, 1)];
      kern_order = [F; Y; H];
      };
    *)
    (* Y = [5, 8]; F = 2  / W = 3 *)
    {varying_dim = Y; range = 5, 8;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    (*
    (* Y = [4, 9]; F = 3  / W = 3 *)
    {varying_dim = Y; range = 3, 9;
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 3)];
      kern_order = [F; Y; W];
      };
    *)
    (* Y = [4, 9]; F = 4  / W = 3 *)
    {varying_dim = Y; range = 4, 9;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    (*
    (* Y = [4, 9]; F = 4  / W = 3 *)
    {varying_dim = Y; range = 3, 5;
      base_kern = [(F, 48); (C, 1); (X, 1); (H, 1); (W, 3)];
      kern_order = [F; Y; W];
      };
    *)
    (*
    (* Y = [4, 5]; F = 5  / H = 3 / W = 3 *)
    {varying_dim = Y; range = 3, 7;
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 3)];
      kern_order = [F; Y; W];
      };
    *)
  ]
end

module Xeon: Arch_info_t = struct
  (* CHANGE SIZES *)
  let l1_size = 8192
  let l2_size = 65536
  let l3_size = 2097152

  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A =  Arch.Nehalem

  let ukernels = [
    (* varying on Y *)
    (* Y = [4, 4]; F = 2 *)
    {varying_dim = Y; range = 4, 5;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* W = 1 H = 3 *)
    (* Y = [5, 8]; F = 1 *)
    {varying_dim = Y; range = 8, 13;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [5, 8]; F = 2 *)
    {varying_dim = Y; range = 4, 7;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* W = 3 H = 1 *)
    (* Y = [8, 15]; F = 2  *)
    {varying_dim = Y; range = 4, 7;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    {varying_dim = Y; range = 3, 4;
     base_kern = [(F, 64); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
  ]
end

module Stephane_avx2_info : Arch_info_t = struct
  (* CHANGE SIZES *)
  let l1_size = 8192
  let l2_size = 262144
  let l3_size = 5046272

  let l1_bw = 8.
  let l2_bw = 8.
  let l3_bw = 0.75

  module A =  Arch.Haswell

  let ukernels = [
    (* varying on Y *)
    (* Y = [8, 14]; F = 2 *)
    {varying_dim = Y; range = 8, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [5, 8]; F = 4 *)
    {varying_dim = Y; range = 5, 8;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 1)];
     kern_order = [F; Y];
    };
    (* Y = [8, 15]; F = 2 / H = 3 *)
    {varying_dim = Y; range = 4, 14;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [3, 8]; F = 4 / H = 3 *)
    {varying_dim = Y; range = 3, 8;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 3); (W, 1)];
     kern_order = [F; Y; H];
    };
    (* Y = [5, 8]; F = 2  / W = 3 *)
    {varying_dim = Y; range = 5, 8;
     base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
    (* Y = [4, 9]; F = 4  / W = 3 *)
    {varying_dim = Y; range = 4, 9;
     base_kern = [(F, 32); (C, 1); (X, 1); (H, 1); (W, 3)];
     kern_order = [F; Y; W];
    };
  ]
end

module Broadwell : Arch_info_t = struct
  (* CHANGE SIZES *)
  let l1_size = 8192
  let l2_size = 65536
  let l3_size = 9175040

  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A =  Arch.Haswell

  let ukernels =
    [{ varying_dim = Y; range = (8, 14);
       base_kern = [(F, 8); (C, 1); (X, 1); (H, 1); (W, 1)];
       kern_order = [F; Y] };
     { varying_dim = Y; range = (4, 12);
       base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 1)];
       kern_order = [F; Y]
     };
     { varying_dim = Y; range = (4, 8);
       base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 1)];
       kern_order = [F; Y]
     };
     { varying_dim = Y; range = (4, 6);
       base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 3)];
       kern_order = [F; Y; W] };
     { varying_dim = Y; range = (7, 14);
       base_kern = [(F, 8); (C, 1); (X, 1); (H, 3); (W, 1)];
       kern_order = [F; Y; H] };
     { varying_dim = Y; range = (4, 5);
       base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
       kern_order = [F; Y; H] }]

end

module E52630 : Arch_info_t = struct
  let l1_size = 8192
  let l2_size = 65536
  let l3_size = 5242880

  let l1_bw = 4.
  let l2_bw = 2.
  let l3_bw = 1.

  module A = Arch.Haswell

  let ukernels = [
    { varying_dim = Y; range = (8, 14);
      base_kern = [(F, 8); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y] };
    { varying_dim = Y; range = (4, 8);
      base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y]
    };
    { varying_dim = Y; range = (4, 4);
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 1)];
      kern_order = [F; Y]
    };
    { varying_dim = Y; range = (4, 6);
      base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 3)];
      kern_order = [F; Y; W] };

    { varying_dim = Y; range = (4, 4);
      base_kern = [(F, 24); (C, 1); (X, 1); (H, 1); (W, 3)];
      kern_order = [F; Y; W] };

    { varying_dim = Y; range = (7, 12);
      base_kern = [(F, 8); (C, 1); (X, 1); (H, 3); (W, 1)];
      kern_order = [F; Y; H]
    };

    { varying_dim = Y; range = (4, 5);
      base_kern = [(F, 16); (C, 1); (X, 1); (H, 3); (W, 1)];
      kern_order = [F; Y; H] }
  ]
end

open Batteries
open Common
open Utils

open Execution

open Conv_search_type
include Modules_decl_search


module S(A_info : Arch_info_t) = struct
  let rec scanl f init = function h::t -> let acc = f init h in acc :: (scanl f acc t) | [] -> []

  let floats_to_cache_line fnum = fnum / 4
  let cache_line_size = 16
  let cache_line_sizef = float_of_int cache_line_size

  let ceil_cache_line inner_size =
    int_of_float @@ Float.ceil (float_of_int inner_size /. cache_line_sizef)
    |> Int.max 1

  let input_footprint _f c y x h w  =
    (x + w - 1) * (y + h - 1) * (ceil_cache_line c)

  let param_footprint f c _y _x h w  =
    w * h * c * (ceil_cache_line f)

  let output_footprint f _c y x _h _w  =
    x * y * (ceil_cache_line f)

  type footprint = {input : int; params: int;
                    output : int; global: int;
                    volume : int;
                   } [@@deriving show {with_path = false}]


  let tensor_compute f c y x h w =
    let apply fn = fn f c y x h w in
    let input = apply input_footprint
    and params = apply param_footprint
    and output = apply output_footprint in
    let global = input + params + output in
    let volume = input + params + 2 * output in
    {input; params; output; global; volume}

  type fp = Tile of int
          | Split of int * int
          | Merge of  int * int [@@deriving show {with_path = false}]

  type fact = Simpl of int | Pair of int * int

  (* Multi-split *)
  let get fp_list dim = List.assoc dim fp_list


  let init_dim_ones = List.map (fun d -> d, 1) enum_dim

  let init_fp = List.map (fun d -> d, Simpl 1) enum_dim

  let map_fp d g =
    List.map (fun (d', f) -> if equal_dim d d' then d', g f else (d', f))

  let footprint  =
    scanl (fun fp (d, f) -> match f with
        | Tile f ->
          map_fp d (function Pair (f1, f2) -> Pair (f * f1, f * f2)
                           | Simpl f' -> Simpl (f' * f)
            ) fp
        | Split (f1, f2) ->
          map_fp d (function Pair _ -> failwith "Already split"
                           | Simpl f -> Pair (f1 * f, f2 * f)
            ) fp
        | Merge (u1, u2) ->
          map_fp d (function Simpl _ -> failwith "Not split"
                           | Pair (f1, f2) -> Simpl (f1 * u1 + f2 * u2)
            ) fp
      ) init_fp

  (* Auguste-like Data volume *)
  let footprint_split  =
    let update_list d f l =
      map_fp d (( * ) f) l in
    let init_dim_split d f1 f2 =
      (update_list d f1 init_dim_ones, update_list d f2 init_dim_ones) in
    let update_right (l1, l2) (d, f) = match  f with
        Merge (f1, f2) | Split (f1, f2) ->
        update_list d f1 l1,
        update_list d f2 l2
      | Tile f ->
        update_list d f l1,
        update_list d f l2 in
    function
    | Left [] | Right [] -> failwith "Empty"
    | Right ((_, Merge _) :: _)
    | Left ((_, Merge _) :: _) -> failwith "Merge should not be first"
    | Left ((_, Split _) :: _) -> failwith "Found a split in left"
    | Left ((d, Tile f) :: remaining) ->
      scanl (fun fp (d, f) ->
          match  f with
            Merge _ | Split _ -> failwith "Error"
          | Tile f -> update_list d f fp
        ) (update_list d f init_dim_ones) remaining
      |> Either.left
    | Right ((d, Tile f) :: remaining) ->
      scanl update_right (init_dim_split d f f) remaining
      |> List.split |> Either.right
    | Right ((d, Split (f1, f2)) :: remaining) ->
      scanl update_right (init_dim_split d f1 f2) remaining
      |> List.split |> Either.right

  let fp_compute fp_list =
    let get = get fp_list in
    let f, c, y, x, h, w = get F, get C, get Y, get X, get H, get H in
    tensor_compute f c y x h w

let level_dim_mul level1 level2 =
    List.fold_left2 (fun prod (d1, l1) (d2, l2) ->
      assert (equal_dim d1 d2);  prod * (l2 / l1))
      1 level1 level2

  let all_footprint =  function
      Left l -> Left (List.map fp_compute l)
      | Right (l1, l2) -> Right (List.map fp_compute l1, List.map fp_compute l2)

  let out_of_cache cache_size {global;_} =
   global >= cache_size

  let data_volume_cache_level cache_size levels =
    let volume level = let {volume;_} = fp_compute level in volume in
    let compute_list loops =
      let l_in_cache, l_out_cache =
        List.span (Bool.not % out_of_cache cache_size % fp_compute) loops in
      match l_out_cache with
      | [] ->  List.last l_in_cache  |> volume
      | (first_out :: nexts) ->
        match List.Exceptionless.last nexts
        with
          Some last_level -> level_dim_mul first_out last_level * volume first_out
        | None  ->   volume first_out in
    match levels with
      Left loops -> compute_list loops
    | Right (loops_left, loops_right) ->
      compute_list loops_left + compute_list loops_right

  let global {global;_} = global

  let compute_footprint fp_list =
    let get = get fp_list %> function Pair (u1, u2) -> [u1; u2]
                                    | Simpl f -> [f] in
    let fl, cl, yl, xl, hl, wl = get F, get C, get Y, get X, get H, get H in
    let all_comb = cartesian [fl; cl; yl; xl; hl; wl] in
    let map_tensor_footprint fp = List.map (function [f; c; y; x; h; w] ->
        fp f c y x h w
                                                   | _ -> assert false)
        all_comb in
    let input = map_tensor_footprint input_footprint
    and params =  map_tensor_footprint param_footprint
    and output = map_tensor_footprint output_footprint  in
    let rec map3 f l1 l2 l3 = match l1, l2, l3 with
      | [], [], [] -> []
      | h1::t1, h2::t2, h3::t3 -> (f h1 h2 h3)::(map3 f t1 t2 t3)
      | _ -> failwith "Lists are not of equal lengths" in
    map3 (fun input params output ->
        let global = input + params + output in
        let volume = input + params + 2 * output in
        {input; params; output; global; volume}
      ) input params output

  let tensor_footprint =
    footprint %> List.map compute_footprint

  let split_footprint =
    scanl


  let gen_perm dim_split (perm : dim list) (f,c,y,x,h,w)  =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let size d = List.assoc d pair_sizes in
    let scheme_dim d = match dim_split with
      | Some (dim, a, b) when equal_dim dim d ->
        let count_d = List.length (List.filter (equal_dim d) perm) - 1 in
        Arithmetic.decompose (size d) count_d
        |> List.concat_map (insert_anywhere (Merge (a, b)) % (List.map (fun x -> Tile x)))
        |> List.map (fun l -> d, l)
      | Some _
      | None ->
        let count_d = List.length @@ List.filter (equal_dim d) perm in
        List.map (fun l -> d, List.map (fun x -> Tile x) l) @@ Arithmetic.decompose (size d) count_d
    in
    let build_scheme perm decomp_list =
      let rec build_scheme acc perm decomp_list = match perm with
        | d::t ->
          let (next_f, other_f), dcmp = remove_map
              (fun (d', l) -> if equal_dim d d'
                then match l with
                    h::t -> Some (h, t)
                  | [] -> failwith "Decomposition is invalid"
                else  None
              ) decomp_list in
          build_scheme ((d, next_f)::acc) t ((d, other_f)::dcmp)
        | [] -> List.rev acc in
      build_scheme [] perm decomp_list in
    List.map (build_scheme perm) (cartesian @@ List.map scheme_dim enum_dim)

  let filter_scheme
      perm_l1 perm_l2 perm_l3 perm_mem
      {varying_dim; base_kern;_} parameters (f,c,y,x,h,w) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let map_size d = match List.assoc_opt d base_kern with
      | Some kern_s -> let size = List.assoc d pair_sizes in
        if (size mod kern_s <> 0)
        then None
        else Some (size / kern_s)
      | None ->
        assert (equal_dim d varying_dim);
        let size = List.assoc d pair_sizes in
        let open Either in
        begin match parameters with
          | Left f ->
            if (size mod f <> 0)  then
              None else Some (size / f)
          | Right (a, u, b, v) ->
            let f = a * u + b * v in
            if (size mod f <> 0)  then
              None
            else  Some (size / f)
        end in
    let sizes = List.fold_right (fun d acc ->
        Option.bind acc (fun l -> Option.map (fun x -> (d, x)::l) (map_size d)))
        enum_dim (Some []) in
    match sizes with
      None -> []
    | Some sizes ->
      let _tvarying, dim_split = match parameters with
          Right (iter1, arg1, iter2, arg2) ->
          Split(arg1, arg2), Some (varying_dim, iter1, iter2)
        | Left f -> Tile f, None in
      let resplit_scheme scheme =
        let l1_perm, remain = List.takedrop (List.length perm_l1) scheme in
        let l2_perm, remain = List.takedrop (List.length perm_l2) remain in
        let l3_perm, mem = List.takedrop (List.length perm_l3) remain in
        l1_perm, l2_perm, l3_perm, mem in
      let get d = List.assoc d sizes in
      gen_perm  dim_split (perm_l1 @ perm_l2 @ perm_l3 @ perm_mem)
        (get F, get C, get Y, get X, get H, get W)
      |> List.map resplit_scheme


  let check_ukernel pair_sizes
      {varying_dim; base_kern; range = rmin, _;  _}  =
    let check_size (dim, size) =
      if equal_dim dim varying_dim then
        size >= rmin
      else
        size >= List.assoc dim base_kern  in
    List.for_all check_size pair_sizes

  let gen_perm_opt sizes ukern =
    let open Ioopt in
    (ioopt_permutation_cached "perm_cache.json" (module A_info) sizes ukern)


  (* Find all permutations *)
  let gen_all_perm_sizes specs =
    let open Ioopt in
    let open Conv_spec in
    List.cartesian_product specs A_info.ukernels
    |> List.filter (fun ({adapted_size = f,c,y,x,h,w;_}, ukern) ->
        let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
        check_ukernel  pair_sizes ukern
      )
  |> Parmap.(fun l -> L l)
    |> Parmap.parmap (fun ({ adapted_size;_}, ukern) ->
        let params = build_ioopt_params (module A_info) adapted_size ukern
        and perm = ioopt_permutation (module A_info) adapted_size ukern in
        params, perm 
      )


  let read_perm_from_file filename =
    let open Ioopt in
    read_from_file filename
    |> print_endline % [%show: (ioopt_params * perm) list]

  let gen_all_perms ukernels ((f,c,y,x,h,w) as sizes) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    ukernels
    |> List.filter (check_ukernel pair_sizes)
    |> List.map (fun ukernel -> ukernel, gen_perm_opt sizes ukernel)

  let gen_schemes ukernels ((f,c,y,x,h,w) as sizes) perms =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    ukernels
    |> List.filter (check_ukernel pair_sizes)
    |> List.concat_map
      ( fun ({range = rmin, rmax; _} as ukern) ->
          let perm_l1, perm_l2, perm_l3, perm_mem =
            Option.default_delayed (fun () -> gen_perm_opt sizes ukern) perms in
          range rmin rmax
          |> List.concat_map
            (fun ds ->
               let parameters = Either.Left ds in
               filter_scheme
                 perm_l1 perm_l2 perm_l3 perm_mem
                 ukern parameters sizes
               |> List.map (fun l -> ukern, parameters, l)
            )
      )

  let ordered_pair l =
    let rec loop acc = function
        h::t -> let ph = List.map (fun x -> (h, x)) t in loop (acc @ ph) t
      | [] -> acc in
    loop [] l

  let intersect eq l1 l2 = List.filter (fun x -> List.exists (eq x) l2) l1

  let gen_pair ukernels ((f,c,y,x,h,w) as sizes) perms  =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let var_divisors var_dim = Arithmetic.all_divisor @@ List.assoc var_dim pair_sizes in
    ukernels
    |> List.filter (check_ukernel pair_sizes)
    |> List.concat_map (fun ({varying_dim; range = rmin, rmax; _} as ukern)  ->
        let perm_l1, perm_l2, perm_l3, perm_mem =
            Option.default_delayed (fun () -> gen_perm_opt sizes ukern) perms in
        range rmin rmax
        |> ordered_pair
        |>  List.concat_map (fun (arg1, arg2) ->
            match List.find_map_opt (Arithmetic.pair_dec arg1 arg2)
                    (var_divisors varying_dim) with
              None -> []
            | Some (iter1, iter2) -> (* iter1 * arg1 + iter2 * arg2 | y *)
              let parameters = Either.Right (iter1, arg1, iter2, arg2) in
              filter_scheme
                perm_l1 perm_l2 perm_l3 perm_mem
                ukern parameters (f, c, y, x, h, w)
              |> List.map (fun l -> ukern, parameters, l)
          )
      )

  let data_volumes_l1_l2_l3 ({varying_dim; kern_order;  base_kern;_}, parameters,
                             (l1, l2, l3, mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          d, Split (arg1, arg2)
        | Left f -> (d, Tile f)
      else (d,  Tile (List.assoc d base_kern)) in
    let ukernel = List.map build_ukernel kern_order in
    let scheme =
      let scheme = ukernel @ l1 @ l2 @ l3 @ mem in
      match parameters with
        Left _ -> Left scheme
      | Right _ -> Right scheme in
    let cache_volume lsize =
      footprint_split scheme |> data_volume_cache_level lsize  in
    cache_volume (floats_to_cache_line A_info.l1_size),
    cache_volume (floats_to_cache_line A_info.l2_size),
    cache_volume (floats_to_cache_line A_info.l3_size)

  let data_volume_weighted implem =
    let l1, l2, l3 = data_volumes_l1_l2_l3 implem in
    let foi = float_of_int in
    Float.(foi l1 / A_info.l1_bw + foi l2 / A_info.l2_bw + foi l3 / A_info.l3_bw)



  (* Get kernel occupation *)
  let cache_occupation ({varying_dim;  base_kern; _}, parameters,
                        (l1, l2, l3, _mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          Some (d, Split (arg1, arg2))
        | Left f -> Some (d, Tile f)
      else match List.assoc d base_kern with
          1 -> None
        | n -> Some (d, Tile n) in
    let ukernel = List.filter_map build_ukernel enum_dim in
    let to_l1 = ukernel @ l1 in
    let to_l2 = to_l1 @ l2 in
    let to_l3 = to_l2 @ l3 in
    let cache_occupation cache_size footprints =
      let foi = float_of_int in
      List.min_max
      @@ List.map (fun fp -> 100. *. (foi (global fp)) /. foi cache_size)
        footprints in
    let min_max_occup cache_size tile_choices =
      cache_occupation cache_size (List.last @@ tensor_footprint tile_choices)
    in
    min_max_occup A_info.l1_size to_l1,
    min_max_occup A_info.l2_size to_l2,
    min_max_occup A_info.l3_size to_l3

  let red_size red_dim (_, _,
                        (l1, l2, l3, mem)) =
    let (d, size) = List.hd l1  in
    if not (equal_dim red_dim d)
    then failwith
        (let sp = [%show: (dim * fp) list] in
         Printf.sprintf
           "Faulty permutation : Dim after microkernel is %s and not %s\n%s %s %s %s\n"
           (show_dim d) (show_dim red_dim)
           (sp l1) (sp l2) (sp l3) (sp mem)) ;
    size |> function Tile s -> s
                   | _ -> assert false

  let metric_conv scheme_spec =
    let red_size = red_size C scheme_spec
    and ((l1_min, l1_max), _, _) = cache_occupation scheme_spec in
    red_size, l1_min, l1_max

  let compare_metrics_lexi _problem_red_size scheme_spec1 scheme_spec2 =
    let goal = 120. in
    let goal_metric l1min l1max =
      Float.abs (goal -. l1min) +. Float.abs (goal -. l1max) in
    let rs1, l1min1, l1max1 = metric_conv scheme_spec1
    and rs2, l1min2, l1max2 = metric_conv scheme_spec2 in
    [%ord: int * float] (Int.neg rs1, goal_metric l1min1 l1max1)
      (Int.neg rs2, goal_metric l1min2 l1max2)

  let compare_metrics problem_red_size scheme_spec1 scheme_spec2 =
    (* rule out small reduction if possible *)
    let p rs1 rs2 =
      let maxr, minr = max rs1 rs2, min rs1 rs2 in
      if (problem_red_size >= 16) && (maxr >= 16 && minr < 16)
      (* if one of them is
         inferior to 16,
         then higher is
         better - put lowest
         score to low red *)
      then Some (Int.compare rs2 rs1)
      (* Else we decide normally *)
      else None in
    let goal = 120. in
    let goal_metric l1min l1max =
      Float.abs (goal -. l1min) +. Float.abs (goal -. l1max) in
    let rs1, l1min1, l1max1 = metric_conv scheme_spec1
    and rs2, l1min2, l1max2 = metric_conv scheme_spec2 in
    let cachem1 = goal_metric l1min1 l1max1
    and  cachem2 = goal_metric l1min2 l1max2 in
    match p rs1 rs2 with
      Some cmp -> cmp
    | None ->
      if Float.abs (cachem1 -. cachem2) >= 100. then Float.compare cachem1 cachem2
      else [%ord: int * float] (Int.neg rs1, goal_metric l1min1 l1max1)
          (Int.neg rs2, goal_metric l1min2 l1max2)
end

module Schemes(A_info : Arch_info_t) = struct
  module Tile = Codegen.Tile(A_info.A)

  open Tile.Conv

  open S(A_info)
  let from_dim = function
    | F -> f_dim
    | C -> c_dim
    | Y -> y_dim
    | X -> x_dim
    | H -> h_dim
    | W -> w_dim

  let to_dim d =
    let open Tensor_loops.Ids in
    let eq = Dim.equal in
    match eq x_dim d, eq y_dim d, eq w_dim d, eq h_dim d, eq c_dim d, eq f_dim d with
      true, _, _, _, _, _ -> X
    | _, true, _, _, _, _ -> Y
    | _, _, true, _, _, _ -> W
    | _, _, _, true, _, _ -> H
    | _, _, _, _, true, _ -> C
    | _, _, _, _, _, true -> F
    | _ -> assert false

  let gen_all_perms = gen_all_perms
  let gen_all perms sizes =
    gen_schemes A_info.ukernels sizes perms
    @ gen_pair A_info.ukernels sizes perms

  let random_select seed num_candidates l =
    (* TODO for reproducibility, set seed to a known value or dump seed value *)
    Random.init seed;
    let num_candidates = min (List.length l) num_candidates in
    Random.multi_choice num_candidates (List.enum l) 
    |> List.of_enum

  let sort_and_select c num_candidates l =
    List.sort (compare_metrics_lexi c) l
    |> List.take num_candidates

  let sort_and_select_volume num_candidates l =
    List.sort (Utils.compare_by data_volume_weighted) l
    |> List.take num_candidates

  let select_mixed min_threshold max_threshold ratio implems =
    let n_candidates = List.length implems in
    let num_select = Int.min n_candidates
        (Int.max min_threshold (ratio * n_candidates / 100)) in
    let implemsi = List.mapi (fun i c -> i, c) implems in
    let best_red = Utils.sort_by (fun (_, c) -> Int.neg @@ red_size C c) implemsi
                   |> List.take num_select in
    let best_red_idxs = List.map fst best_red in
    List.map (List.at implems) best_red_idxs
    |> Utils.sort_by data_volume_weighted
    |> List.take max_threshold

  let cache_occupation = cache_occupation

  let to_scheme ({varying_dim; kern_order; base_kern;_}, parameters,
                 (l1, l2, l3, mem)) =
    let build_ukernel d =  if equal_dim varying_dim d then
        match parameters with
          Left f ->
          Some (U (f, from_dim d))
        | Right _ ->
          Some (ULambda (from_dim d))
      else match List.assoc d base_kern with
          1 -> None
        | n -> if equal_dim F d then Some (U (n / A_info.A.vec_size, from_dim d))
          else Some (U (n, from_dim d)) in
    let ukernel = (V f_dim)::(List.filter_map build_ukernel kern_order) in
    let to_spec (dim, f) = match f with
        Tile f -> T (f, from_dim dim)
      | Split (_, _) -> failwith "Should not encounter Split outside of ukernel"
      | Merge (_, _) ->
        begin match parameters with
            Right (iter1, arg1, iter2, arg2) ->
            Lambda_apply (from_dim dim, [Iter iter1, Arg arg1; Iter iter2, Arg arg2;])
          | Left _ -> assert false
        end in
    let insert_hoist l = match l with
      | T (f, d)::tail when Tensor_loops.Ids.Dim.equal d c_dim ->
        T (f, d) :: Hoist_vars [c_dim] :: tail
      | _ -> failwith "C dim not here ? " in
    List.concat
      [ukernel;
       (List.map to_spec l1 |> insert_hoist) ;
       List.map to_spec l2;
       (*          if do_pack then [Pack_tens params] else []; *)
       List.map to_spec l3;
       List.map to_spec mem;
      ]

  (* TVM style output, to be passed to Stephane *)
  let to_scheme_from_list parameters l =
    let to_spec_ukernel (dim, f) = match f with
        Tile f when equal_dim dim F -> [V f_dim; U (f / A_info.A.vec_size, f_dim)]
      | Tile f  -> [U (f, from_dim dim)]
      | Split (_, _)
      | Merge (_, _) -> assert false
        in
    let to_spec (dim, f) = match f with
        Tile f -> T (f, from_dim dim)
      | Split (_, _) -> ULambda (from_dim dim)
      | Merge (_, _) ->
        begin match parameters with
            Right (iter1, arg1, iter2, arg2) ->
            Lambda_apply (from_dim dim, [Iter iter1, Arg arg1; Iter iter2, Arg arg2;])
          | Left _ -> assert false
        end in
        let before_c, c_and_after =
          let rec loop acc = function
            | (C, s) :: t -> List.rev acc, (C, s) ::t 
            | h :: t -> loop (h::acc) t 
            | [] -> assert false in
          loop [] l in
    let rec insert_hoist l = match l with
      | T (f, d)::tail when Tensor_loops.Ids.Dim.equal d c_dim ->
        T (f, d) :: Hoist_vars [c_dim] :: tail
      | h :: tail -> h :: (insert_hoist tail)
      | [] -> failwith "C is absent" in
        ((List.concat_map to_spec_ukernel before_c) @ List.map to_spec c_and_after)
        |> insert_hoist

  let take_until p init =
    let rec loop l acc = function [] -> List.rev l, []
                                | h::t -> match p acc h with
                                  | Some acc' -> loop (h::l) acc' t
                                  (* Should h go with l or t ? *)
                                  | None -> List.rev (h::l), t in
    loop [] init

  (* Tactic used by stephane *)
  let choose_level_first_tactic l  =
    take_until
      (fun ldims (d,_) ->
         match (List.remove ldims d) with
           [] -> None | l -> Some l)
      [C; H; W] l

  let to_tvm_predicate choose_tile_level ({varying_dim; kern_order; base_kern;_},
              parameters,
              (l1, l2, l3, mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          d, Split (arg1, arg2)
        | Left f -> (d, Tile f)
      else (d,  Tile (List.assoc d base_kern)) in
    let ukernel = List.map build_ukernel kern_order in
    let complete_scheme = ukernel @ l1 @ l2 @ l3 @ mem in
    let update_d d m = List.modify d (( * ) m) in
    let split_scheme l =
      List.map (function (d, Merge (a1, a2))
                       | d, Split (a1, a2) -> (d, a1), (d, a2)
                       | d, Tile t -> (d, t), (d, t)) l
      |> List.split in
    let split_scheme_either l =
      List.fold_left (fun acc (d, factor) -> match acc, factor with
          | Left l, Tile t -> Left (update_d d t l)
          | Right (l1, l2), Tile t -> Right (update_d d t l1, update_d d t l2)
          | Left l, (Split (a1, a2) | Merge (a1, a2)) -> Right (update_d d a1 l, update_d d a2 l)
          | Right (l1, l2), Merge (a1, a2) ->
            let t1 = List.assoc d l1
            and t2 = List.assoc d l2 in
            Left (List.modify d (fun _ -> a1 * t1 + a2 * t2) l1)
          |  Right _, Split _ -> assert false
        ) (Left init_dim_ones) l in
    let l_tensorize, l_out =
      choose_tile_level complete_scheme in
    let size_on_dim d =
      List.fold_left (fun s (d', s') -> if equal_dim d d' then s * s' else s) 1 in
    match split_scheme_either l_tensorize with
      Left ltens ->
      (* Shouldn't be any merge out of ltens in this case *)
      let l_out = List.map (function (_, (Merge _ | Split _)) -> assert false
                                   | d, Tile t -> d, t) l_out in
      [l_out, ltens, size_on_dim Y (l_out @ ltens), to_scheme_from_list parameters l_tensorize]
    | Right (l1, l2) ->
      let l1_tens, l2_tens = split_scheme l_tensorize
      and l1_out, l2_out = split_scheme l_out in
      let into_fp = List.map (map_snd (fun t -> Tile t)) in
      [l1_out, l1, size_on_dim Y (l1_out @ l1), to_scheme_from_list parameters @@ into_fp l1_tens;
       l2_out, l2, size_on_dim Y (l2_out @ l2), to_scheme_from_list parameters @@ into_fp l2_tens]

  let to_tvm = to_tvm_predicate choose_level_first_tactic
end

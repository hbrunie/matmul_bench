open Batteries
open Conv_search_type
open Ppx_python_runtime


type array = {name : string; dims : string  list; multiplier : int} 

type program = {
  dims : dim list;
  arrays : array list;
  reuse : (array * dim list) list;
}

let convolution  =
  let output = {name = "output";
                dims = ["F"; "X"; "Y";];
                multiplier = 2;
               }
  and input = {name = "input";
               dims = [ "C"; "X + W"; "Y + H";];
               multiplier = 1;
              }
  and params = {name = "params";
                dims = [ "F"; "C"; "W"; "H"];
                multiplier = 1;
               } in
  {dims = [F; C; X; Y; W; H];
   arrays = [output; input; params];
   reuse = [output, [C; W; H]; input, [F]; params, [X; Y]]
  }

let py_of_program ({dims; arrays; reuse} : program) =
  let dims = [%python_of: string list] @@  List.map show_dim dims
  and arrays =
    arrays
    |> List.map
      (fun {name; dims; multiplier} ->
         name,
         (dims,
          multiplier)
         |> [%python_of: string  list * int]
      )
    |> Py.Dict.of_bindings_string
  and reuse = List.map
      (fun ({name;_}, dim_list) -> name, [%python_of : string list] @@ List.map show_dim dim_list)
      reuse
              |> Py.Dict.of_bindings_string
  in
  dims, arrays, reuse

let py_of_kern (f,c,x,y,w,h) kernel_class =
  let sizes = [F, f; C, c; X, x; Y, y; W, w; H, h] in
  kernel_repr sizes kernel_class
  |> List.map (fun (d, s) -> show_dim d, Py.Int.of_int s)
  |> Py.Dict.of_bindings_string

(*
type ioopt_params = {l1_size: int; l2_size : int; l3_size : int;
                     l1_bw: float; l2_bw: float; l3_bw: float;
                     size: int * int * int * int * int * int;
                     kernel_class : kernel_spec;
                     name :string;
                    } [@@deriving show {with_path = false}, eq, yojson]
*)

(* same struct without name *)
type ioopt_params = {l1_size: int; l2_size : int; l3_size : int;
                     l1_bw: float; l2_bw: float; l3_bw: float;
                     size: int * int * int * int * int * int;
                     kernel_class : kernel_spec;
                    } [@@deriving show {with_path = false}, eq, yojson]


let build_ioopt_params (module Ainfo : Arch_info_t) size kernel_class  =
  let open Ainfo in
  {size; kernel_class; l1_size; l2_size; l3_size; l1_bw; l2_bw; l3_bw}

(*
let forget_name {size; kernel_class; name = _; l1_size; l2_size;
                 l3_size; l1_bw; l2_bw; l3_bw}: ioopt_params_no_name =
  {size; kernel_class; l1_size; l2_size; l3_size; l1_bw; l2_bw; l3_bw}
*)
  

type perm = dim list * dim list * dim list * dim list
[@@deriving show {with_path = false}, eq, yojson]

let read_from_file filename =
  Yojson.Safe.from_file filename
  |> [%of_yojson: (ioopt_params * perm) list]
  |> Result.get_ok


let write_to_file dictname params_perm =
  [%to_yojson: (ioopt_params * perm) list ] params_perm
  |> Yojson.Safe.to_file dictname

let py_build_values (module Ainfo : Arch_info_t) (f,c,x,y,w,h) kernel_class =
  let open Ainfo in
  let lsizes  = "cache", [%python_of: int list] [l1_size; l2_size; l3_size]
  and bw_list = "bw", [%python_of: float list] [l1_bw; l2_bw; l3_bw]
  and dims = "dims", [%python_of: int list] [f;c;x;y;w;h]
  and ukernel = "microkernel",
                py_of_kern  (f,c,x,y,w,h) kernel_class
  and n_levels = "n_levels", Py.Int.of_int 3 in
  Py.Dict.of_bindings_string [lsizes; bw_list; dims; ukernel; n_levels]

let py_build_params program ainfo sizes kernel_class =
  let dims, arrays, reuse = py_of_program program
  and values = py_build_values ainfo sizes kernel_class in
  Py.Dict.of_bindings_string
    ["dims", dims; "arrays", arrays; "reuse", reuse; "values", values]


let tee f x = f x; x

let ioopt_permutation ainfo sizes kernel_class =
  let () = if Py.is_initialized () then () else Py.initialize () in
  let ioopt = Py.import "ioopt" in
  let open Pyops in
  let params = py_build_params convolution ainfo sizes kernel_class in
  let ukernel = py_of_kern sizes kernel_class in
  let l1, l2, l3, mem =
    ioopt.&("get_perm_from_microkernel") [|params; ukernel|]
    |> [%of_python: string list list]
    |> List.map (List.map dim_from_string)
    |> (function [l1; l2; l3; mem] -> l1, l2, l3, mem
               | _ -> failwith "Invalid ioopt output") in
  (* Make sure C is the first dimension around the ukernel *)
  let add_c = function C::_ as l -> l | l -> C::l in
  add_c l1, l2, l3, mem

let ioopt_permutation_cached cachefile ainfo sizes kernel_class =
  let params = build_ioopt_params ainfo sizes kernel_class
  and params_perm = read_from_file cachefile in
  match List.assoc_opt params params_perm with
  | Some perm -> perm
  | None -> ioopt_permutation ainfo sizes kernel_class

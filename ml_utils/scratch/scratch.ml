open Batteries
open Common
open Execution
open Bench_types 


module A = Arch.Haswell

open Bench(A)
module Tile = Codegen.Tile(A)
module Pred_conv = Arch.Pred_conv(A)

let exec_scheme counters_list (f, c, x, y, h, w) stride scheme  =
  let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv;] counters_list in
  let args = match stride with None -> Config.conv_args x w y h c f
                             |Some (sx, sy) ->
                               Config.conv_args_stride x w y h c f sx sy
  in
  let res = (exec conf_gen args
               (Tile.Conv_tile scheme)) in
  List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
    counters_list


let mb_scheme = Tile.Conv.[V f_dim; U (1, f_dim); ULambda y_dim; U (3, h_dim); T (1, c_dim);
                        Hoist_vars [c_dim]; T (28, x_dim);
              T (32, c_dim); T (3, w_dim); T (4, x_dim); T (2, y_dim); T (2, f_dim);
              Lambda_apply (y_dim, [((Iter 5), (Arg 9)); ((Iter 1), (Arg 11))])]

(*
let () = let open Conv_sizes in
  exec_scheme Counter.[CYCLES] (16, 128, 2, 8, 3, 3)  mobilNet_3_stride []
  |> ignore
*)

let () =
  let scheme = Tile.Conv.[V f_dim; U (2, f_dim); U (5, y_dim); U (3, w_dim); T (256, c_dim);
                          Hoist_vars [c_dim]; ] in
  let ds = Tile.Conv.dim_tile_size scheme in
  let f,c,y,x,h,w = Tile.Conv.(ds f_dim, ds c_dim, ds y_dim, ds x_dim, ds h_dim, ds w_dim) in
  Printf.printf "f %d,c %d, y %d,x %d,h %d,w %d\n" f c y x h w ;
  exec_scheme Counter.[CYCLES]
    (f,c,x,y,h,w) Conv_sizes.mobilNet_3_stride scheme
  |> List.assoc Counter.CYCLES
  |> Utils.tee (Printf.printf "%d cycles\n")
  |> Pred_conv.peak_percent f c y x h w
  |> Printf.printf "%f%%\n"

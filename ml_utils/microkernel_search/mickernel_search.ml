open Batteries
open Common
open Execution
open Bench_types


(* Microkernel exploration (ICS 2021 experiments) *)
(* Authors: Guillaume & Hugo *)


(* This module is parametrized over Architecture. It contains a whole bunch of
 * experiments. This is not stable and is not intended to be. Any function
 * considered worth being stabilized should be moved to another file - maybe
 * data_process. *)
module Expe(A:Arch.Arch_t) = struct
  open Execution
  module Tile = Codegen.Tile(A)
  module MM = Tile.MM
  module Conv = Tile.Conv
  include Bench(A)
  include Config
  module Pred_mm = Arch.Pred_mm(A)
  module Pred_conv = Arch.Pred_conv(A)

  let sizes_from_tile_mm tile = let open MM in
    let map3 f x y z = f x, f y, f z in
    map3 (MM.dim_tile_size tile) i_dim j_dim k_dim

  let sizes_from_tile_conv tile = let open Conv in
    let map6 f x y z a b c = f x, f y, f z, f a, f b, f c in
    map6 (Conv.dim_tile_size tile)  x_dim w_dim y_dim h_dim c_dim f_dim

end

(* ========================================== *)

(* January 27 2021 *)

(* Test from Hugo *)
let test_microkernels_matmul (module A : Arch.Arch_t) x y =
  let x_ = x * 8 in   (* Vectorization : 8 float in a 256 vector *)
  let ksize = 512 in (* 256 128 *)

  let open Expe(A) in
  let tile = Tile.MM.([ V j_dim; U (x, j_dim); U (y, i_dim); T ( ksize, k_dim); R i_dim; R j_dim; R k_dim]) in
  let isize, jsize, ksize = sizes_from_tile_mm tile in
  let conf = build_mm_config ~error:(Config.EPSILON 0.00001) (Some (true, true, Gcc,  O3))
      [Gen] [CYCLES] in
  let module Pred_mm = Arch.Pred_mm(A) in
  exec conf (mm_args y x_ ksize) (Tile.MM_tile tile)
  |> gen_cycles 
  |> Pred_mm.peak_percent isize jsize ksize
  |> (fun res -> (res, x, y))
  (* |> Printf.printf "peak percent (X = %d | Y = %d) : %.8f\n" x y *)
  (* |> Config.show_results |> print_endline *)

(* Try all µkernels for matmul *)
let test_all_microkernels_matmul arch =
  let generate_configs (rmin, rmax) (pmin, pmax) =
    (* Sorry but this code is much much clearer in sequential *)
    let lconfig = ref [] in
    for x = rmin to rmax do
      for y = rmin to rmax do
        if ((pmin <= x * y) && (x * y <= pmax)) then
          lconfig := (x,y)::(!lconfig)
      done
    done;
    !lconfig
  in

  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 4 in
  let prod_max = 30 in

  let config_x_y = generate_configs (range_min, range_max) (prod_min, prod_max) in
  let config_x_y = List.rev config_x_y in   (* More confort in reading the results *)

  (* DEBUG
  let rec print_configs configs = match configs with
    | [] -> ()
    | (x,y)::r -> (
      Printf.printf "\t - x = %d | y = %d\n" x y;
      print_configs r
    )
  in
  print_configs config_x_y; *)

  let lres_x_y = List.map (fun (x,y) ->
    test_microkernels_matmul arch x y
  ) config_x_y in

  let comp_sorting (res1,_,_) (res2,_,_) =
    if (res1==res2) then 0 else
    if (res1<res2) then (-1) else
    1
  in
  let lres_x_y = List.sort comp_sorting lres_x_y in

  List.iter (fun (res,x,y) ->
    Printf.printf "peak percent (X = %d | Y = %d) : %.8f\n" x y res
  ) lres_x_y;
  Printf.printf "DONE !\n";
  ()


(* ===== *)

(* Microkernel for convolution *)
let test_microkernels_conv (module A : Arch.Arch_t) size_h size_w size_f size_y =
  let open Expe(A) in
  let open Conv in
  (* let open Gen in *)

  let size_f_ = size_f * A.vec_size in
  let size_c = 512 in (* 256 128 *)  (* Streaming dimension*)

  let size_x = 1 in

  let strat = [ V f_dim;
                U (size_f, f_dim); U (size_y, y_dim); U (size_h, h_dim); U (size_w, w_dim);
                T(size_c, c_dim); Hoist_vars [c_dim] ] in  (* b=1 *)
  let args = conv_args size_x size_w size_y size_h size_c size_f_ in (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen = build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv, Conv_layout (XYF, XYC, WHCF); ] [CYCLES]
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score = exec conf_gen args (Tile.Conv_tile strat)
                 |> select_bench_counter Conv CYCLES
                 |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f_
  in
  (gen_score, size_f, size_y, size_h, size_w)



(* Try all µkernels for convolution *)
let test_all_microkernels_conv fun_test =
  let generate_configs (rmin, rmax) (pmin, pmax) =
    (* Sorry but this code is much much clearer in sequential *)
    let lconfig = ref [] in
    for x = rmin to rmax do
      for y = rmin to rmax do
        if ((pmin <= x * y) && (x * y <= pmax)) then
          lconfig := (x,y)::(!lconfig)
      done
    done;
    !lconfig
  in

  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 1 (* 4 *) in
  let prod_max = 30 in

  let config_x_y = generate_configs (range_min, range_max) (prod_min, prod_max) in
  let config_x_y = List.rev config_x_y in   (* More confort in reading the results *)

  (* DEBUG
  let rec print_configs configs = match configs with
    | [] -> ()
    | (x,y)::r -> (
      Printf.printf "\t - x = %d | y = %d\n" x y;
      print_configs r
    )
  in
  print_configs config_x_y; *)

  let lres_sizes = List.concat_map (
      fun (h, w) -> List.map (fun (f,y) ->
      fun_test h w f y
        ) config_x_y)
      [1, 1; 3, 1; 1, 3; 3, 3] in

  let comp_sorting (res1,_,_,_,_) (res2,_,_,_,_) =
    if (res1==res2) then 0 else
    if (res1<res2) then (-1) else
    1
  in
  let lres_sizes = List.sort comp_sorting lres_sizes in

  let x_size = 1 in
  let c_size = 1 in
  Printf.printf "x,y,w,h,c,f,res\n";
  List.iter (fun (res,f,y,h,w) ->
    Printf.printf "%d,%d,%d,%d,%d,%d,%.8f\n" x_size y w h c_size f  res
  ) lres_sizes;
  ()



(* Microkernel for convolution *)
let test_microkernels_combination (module A: Arch.Arch_t) =
  let open Expe(A) in
  let open Conv in

  (* let open Gen in *)

  (* 6*[y=4,h=3,f=2]+2*[y=5,h=3,f=2]  82.739672 predicted ===> 84.87 in practice *)
  let size_f = 2 in
  let size_h = 3 in
  let size_y_ = 6*4 + 2*5 in

  let size_f_ = size_f * A.vec_size in
  let size_c = 512 in (* 256 128 *)  (* Streaming dimension*)

  let size_x = 1 in
  let size_w = 1 in

  let strat = [ V f_dim; U (size_f, f_dim); ULambda y_dim; U (size_h, h_dim); T(size_c, c_dim);
                Hoist_vars [c_dim];
                Lambda_apply (y_dim, [Iter 6, Arg 4; Iter 2, Arg 5]) ;
                R f_dim; R c_dim; R x_dim; R y_dim; R w_dim; R h_dim ] in  (* b=1 *)
  let args = conv_args size_x size_w size_y_ size_h size_c size_f_ in (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES]
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score = exec conf_gen args (Tile.Conv_tile strat)
                 |> Config.select_bench_counter Conv CYCLES
                 |> Pred_conv.peak_percent size_x size_y_ size_w size_h size_c size_f_
  in
  Printf.printf "x,y,w,h,c,f,res\n";
  Printf.printf "%d,(6*4+2*5),%d,%d,%d,%d,%.8f\n" size_x (*y*) size_w size_h 1(*c*) size_f_  gen_score;
  ()

let test_microkernels_single (module A: Arch.Arch_t) =
  let open Execution.Bench(A) in
  let open Expe(A) in
  let open Conv in

  let size_f = 3 in
  let size_h = 1 in
  let size_y = 9 in

  let size_f_ = size_f * A.vec_size in
  let size_c = 512 in (* 256 128 *)  (* Streaming dimension*)

  let size_x = 1 in
  let size_w = 1 in

  let strat = [ V f_dim; U (size_f, f_dim); U (size_y, y_dim); T(size_c, c_dim);
                Hoist_vars [c_dim];
                R f_dim; R c_dim; R x_dim; R y_dim; R w_dim; R h_dim ] in  (* b=1 *)
  let args = conv_args size_x size_w size_y size_h size_c size_f_ in (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES]
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score = exec conf_gen args (Tile.Conv_tile strat)
                 |> Config.select_bench_counter Conv CYCLES
                 |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f_
  in
  Printf.printf "x,y,w,h,c,f,res\n";
  Printf.printf "%d,%d,%d,%d,%d,%d,%.8f\n" size_x size_y size_w size_h 1(*c*) size_f  gen_score;
  ()



(* ========================================== *)
(* ========================================== *)
(* ========================================== *)


(* let () = test_all_microkernels_matmul ()*)

let () = test_all_microkernels_conv 
    (test_microkernels_conv (module Arch.Haswell))
(*   test_microkernels_conv_h_inside*)
(*   test_microkernels_conv_w_inside *)
(*   test_microkernels_conv_h_w_inside *)


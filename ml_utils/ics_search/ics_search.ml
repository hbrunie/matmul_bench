open Batteries
open Common
open Utils
open Execution
open Bench_types

open Conv_spec
(* This module is parametrized over Architecture. It contains a whole bunch of
 * experiments. This is not stable and is not intended to be. Any function
 * considered worth being stabilized should be moved to another file - maybe
 * data_process. *)
module Expe(A:Arch.Arch_t) = struct
  open Execution
  module Tile = Codegen.Tile(A)
  include Bench(A)
  include Config
  module Pred_mm = Arch.Pred_mm(A)
  module Pred_conv = Arch.Pred_conv(A)

  let sizes_from_tile_conv tile = let open Tile.Conv in
    let map6 f x y z a b c = f x, f y, f z, f a, f b, f c in
    map6 (dim_tile_size tile)  x_dim w_dim y_dim h_dim c_dim f_dim

end

open Search

open Conv_search_type

module S(Ainfo : Arch_info_t) = struct
  module A = Ainfo.A
  open Search.Schemes(Ainfo)
  module B = Execution.Bench(Ainfo.A)
  module Pred_conv = Arch.Pred_conv(A)

  open Streaming

  let gen_schemes ((_,c,_,_,_,_) as size) num_candidates =
    gen_all None size
    |> (match num_candidates with
          Some num_candidates -> sort_and_select c num_candidates
        | None -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_scheme

  let exec_scheme counters_list (f, c, x, y, h, w) stride scheme  =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:1
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
    let args = match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy
    in
     Tile.Conv.tile_to_string scheme,
     let res = (B.exec conf_gen args
                  (Tile.Conv_tile scheme)) in
     List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
       counters_list


  let format_first_line counter_list =
    let open Counter in
    List.map (function CYCLES -> "perf"
                     | c -> show c  )
      counter_list
    |> String.concat ", "

  let sort_output stream =
    Stream.into Sink.list stream
    |> List.sort
      (fun (_, res1) (_, res2) ->
         let get_cycles res = List.assoc Counter.CYCLES res in
         Int.compare (get_cycles res1) (get_cycles res2))

  let format_flow_line (f,c,y,x,h,w) counter_list res =
    let open Counter in
    List.map (function
        | CYCLES ->
          (List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
           |> Printf.sprintf "%.2f")
        | c -> List.assoc c res
               |> string_of_int
      ) counter_list
    |> String.concat "," 

  (* Write results to file *)
  let res_write name sizes counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list in
      IO.write_line f first_line; f
    and push file (_, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s\n" line;
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()

  (* Write tile schemes to file *)
  let tile_write name  =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
                    1, File.open_out (loop (A.arch_name ^ "_" ^ name ^ "_tiles" ^
                                       ".log") )
    and push (count, file) (tile, _) =
      Printf.fprintf file "%d : %s\n" count tile;
      IO.flush file;
      count + 1, file
    and stop (_, f) = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow sizes counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.printf "%s\n%s\n" tile line
    and stop () = () in
    Sink.make ~init ~push ~stop ()

  let exec_all counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes adapted_size None
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list <& print_flow adapted_size counter_list  <& tile_write name)
      ) conv_spec

  let exec_and_sort_results counter_list num_candidates conv_spec =
    List.map
      (fun {name; adapted_size; stride;_} ->
         name,
         adapted_size,
         gen_schemes adapted_size (Some num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> sort_output
      ) conv_spec
end

let search_all (module Ainfo: Arch_info_t) names_sizes_strides =
  let open S(Ainfo) in
  let open Counter in
  exec_and_sort_results [CYCLES] 50 names_sizes_strides
  |> List.map
    (fun (name, (f,c,y,x,h,w), l_res) ->
       name,
       (List.map (
           map_snd @@
           List.assoc CYCLES
           %> Pred_conv.peak_percent f c y x h w
         ) l_res
       )
    )

let find_best_and_log (ainfo: (module Arch_info_t)) filename names_sizes_strides =
  let list_bests = search_all ainfo names_sizes_strides in
  let best_of_bests = List.map (map_snd List.hd) list_bests in
  File.with_file_out filename (fun f ->
      Printf.fprintf f "name, perf, best scheme";
      List.iter (fun (name, (tile_scheme, perf)) ->
          Printf.fprintf f "%s, %.2f, %s\n" name perf tile_scheme 
        )
    ) best_of_bests;
  List.iter (fun (name, l_res) ->
      let filename = filename ^ "_" ^ name in
      File.with_file_out filename (fun f ->
          List.iter (fun (tile_scheme, perf) ->
              Printf.fprintf f "%.2f, %s\n" perf tile_scheme 
            ) l_res
        )
    ) list_bests

module S_avx512 = S(Arch_info.Skylake_info)

let all () = S_avx512.exec_all (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    Conv_sizes.all_spec
let ffffff () = S_avx512.exec_all (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    (List.drop 21 Conv_sizes.all_spec)

module S_avx2 = S(Arch_info.Skylake_avx2_info)
let fecv () = S_avx2.exec_all (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]) Conv_sizes.all_spec
let () = find_best_and_log (module Arch_info.Skylake_avx2_info) "res_avx2.log" Conv_sizes.all_spec

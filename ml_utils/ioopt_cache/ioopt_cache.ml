open Batteries
open Common

open Search

module AI = Search.Arch_info

let build_cache () =
  let open Conv_search_type in
  let arch_mods: (module Arch_info_t) list =
    AI.[(module Xeon); (module Silver_info); (module Skylake_avx2_info);
     (module Skylake_info)] in
  List.concat_map (fun (module A_info: Arch_info_t) ->
      let module Sa = S(A_info) in
      Sa.gen_all_perm_sizes Conv_sizes.all_spec
    ) arch_mods 
  |> Ioopt.write_to_file "perm_cache.json"

let complete_cache_yolo23 () =
  let open Conv_search_type in
  let cachefile = "perm_cache.json" in
  let arch_mods: (module Arch_info_t) list =
    AI.[(module Xeon); (module Silver_info); (module Skylake_avx2_info);
     (module Skylake_info)] in
  let params_perm = Ioopt.read_from_file cachefile in
      let module Sa = S(Search.Arch_info.Silver_info) in
  let yolo23_par_perms = List.concat_map (fun (module A_info: Arch_info_t) ->
      let module Sa = S(A_info) in
      Sa.gen_all_perm_sizes [Conv_sizes.yolo9000_23_spec]
    ) arch_mods in
  let new_params_perms = params_perm @ yolo23_par_perms in
  Ioopt.write_to_file "perm_cache_updated.json" new_params_perms

let complete_cache () =
  let cachefile = "perm_cache.json" in
  let params_perm = Ioopt.read_from_file cachefile in
  let module Sa = S(Search.Arch_info.E52630) in
  let silver_perms = Sa.gen_all_perm_sizes Conv_sizes.all_spec in
  let new_params_perms = params_perm @ silver_perms in
  Ioopt.write_to_file "perm_cache_updated_par.json" new_params_perms


open Conv_search_type
let search_class (module A: Arch.Arch_t) csvname =
  let csvlist = Parse_csv.parse_remove_header csvname in
  let open Arch_info in
  search_class A.vec_size 60. @@ List.map to_perfs csvlist
  |> List.iter @@ print_endline % show_kernel_spec

let () = complete_cache ()

open Shexp_process
open Shexp_process.Infix
open Inject_code
open Batteries
open Common
module BC = Build_c
include Modules_decl

module Bench(A:Arch.Arch_t) = struct
  open Codegen
  module Tile_a = Tile(A)

  let string_from_template lines_to_insert mark template =
    let mark_begin =Printf.sprintf ">>INSERT %s>>" mark
    and mark_end =Printf.sprintf "<<INSERT %s<<" mark in
    run "cat" [template]
    |-
    fold_replace_shxp mark_begin mark_end [lines_to_insert]

  let file_from_template lines_to_insert mark template dstfile =
    string_from_template lines_to_insert mark template
    >>= Shexp_process.print |> stdout_to dstfile

  let code_from_template layout arg tile =
    string_from_template (Tile_a.gen_code layout arg tile) "CODE"

  let code_file_from_template layout arg tile =
    file_from_template (Tile_a.gen_code layout arg tile) "CODE"

  let params_from_template conf =
    let src = from_config conf |> gen_params (module A) in
    file_from_template src "PARAMS"

  let handle_counters_from_template counters =
    let src = gen_counter_print counters  in
    file_from_template src "COUNTERS_PRINT"

  let event_names_from_template counters =
    let src = gen_events_name counters  in
    file_from_template src "EVENT_NAMES"

  let discard_stdout command =
    stdout_to "/dev/null" command
    |> stderr_to "/dev/null"

  let home = get_env_exn "HOME"

  (* if generation is None, files are not recompiled *)
  let dry_run ?(discard_out=true) conf args =
    let proceed_command = if discard_out then discard_stdout else Fun.id in
    let open Config in
    let {generation; blis_lib; benches; _} = to_conf conf in
    let exec_args: type a. a args -> unit Shexp_process.t =
      let soi = string_of_int in function
        | MM_args (i, j, k) ->
          run "./mm_bench.x" [soi i; soi j; soi k]
        | Conv_args {x; w; y; h; c; f; strides = None; _} ->
          let args = List.map soi [x; w; y; h; c; f] in
          run "./mm_bench.x" args
        | Conv_args {x; w; y; h; c; f; strides = Some (str_x, str_y); _} ->
          let args = List.map soi [x; w; y; h; c; f; str_x; str_y] in
          run "./mm_bench.x" args in
    let open Build_c in
    home
    >>= fun _ ->
    Option.map_default (fun (_, _, compiler, opt_level) ->
        compile ~opt_level compiler A.include_dirs A.ld_libraries blis_lib  A.arch_name (List.map fst benches)
        |> proceed_command)
      (return ())
      generation
    >> exec_args args

  (* Run process only if cond is true *)
  let may cond process = 
    if cond then (Lazy.force process) else return ()

  let may_opt cond process = match cond with
    | Some x -> (Lazy.force process x)
    | None -> return ()


  let glue_all (type a) (conf: a Config.t) (args : a Config.args) tile =
    let open Config in
    let gen_name: type a. a t -> string = function
      | MM_conf _ -> "gen_matmul.c"
      | Conv_conf _ -> "gen_conv.c" in
    let gen_name = gen_name conf in
    let {generation; benches; counters;_} = Config.to_conf conf in
    let contains_gen_or_conv: type a. a bench * a layout -> (a layout) option = function
      | Gen, MM_layout -> Some (MM_layout)
      | Conv, conv_layout -> Some (conv_layout)
      | _ -> None in
    let layout_opt  = List.find_map_opt contains_gen_or_conv benches in
    let regen_kern: a layout option =
      Option.bind generation (fun (cond,_, _, _) -> if cond then layout_opt else None) in
    let regen_framework = match generation with Some (_, true,_, _) -> true | _ -> false in
    discard_stdout A.init
    (* regenerate code only if regen = true - now that we have added layout this feels unecessary *)
    >> may_opt regen_kern
      (lazy (fun layout -> code_file_from_template layout args tile
                (Printf.sprintf "%s.template" gen_name)
               (Printf.sprintf "gen/%s" gen_name)))
    >> may regen_framework
      (lazy (params_from_template conf "params.h.template" "gen/params.h"
             >> event_names_from_template counters "event_names.h.template"
               "gen/event_names.h"
             >> handle_counters_from_template counters "handle_counters.c.template"
               "gen/handle_counters.c"))
    >> dry_run (*~discard_out:false*)  conf  args
       |- read_all >>|  Config.results conf

  let set_ld_library prog =
    match A.ld_libraries with
    | [] -> prog
    | l ->
      String.concat ":" l
      |> fun s -> set_env "LD_LIBRARY_PATH" s prog

  let set_runtime_vars prog =
    List.fold_left (fun prog (name, value) ->
    set_env name value prog)
      prog A.runtime_vars

  let gen_code_file layout args tile filename postfix =
    code_file_from_template layout args tile
      (Printf.sprintf "%s.c.template" filename)
      (Printf.sprintf "gen/%s.c" (filename ^ postfix))

  let filename:  type a. a Config.args -> string = function
    | Config.MM_args _ -> "gen_mm"
    | Config.Conv_args _ -> "gen_conv"

  let gen_code layout args tile =
    let context = Context.(create ~cwd:(Path "../c_bench") ()) in
    let expr =
      code_from_template layout args tile
      (Printf.sprintf "%s_no_include.c.template" @@ filename args) in
    eval ~context @@ expr

  let gen_file layout args tile postfix =
    let context = Context.(create ~cwd:(Path "../c_bench") ()) in
    eval ~context @@ gen_code_file layout args tile (filename args) postfix

  let exec conf args tile =
    let context = Context.(create ~cwd:(Path "../c_bench") ()) in
    (* If dispose is not called at some point, we'll have a resource leak (too
     * many files opened) *)
    Gc.finalise Context.dispose context;
  let  glue = glue_all conf args tile
            |> set_ld_library
            |> set_env "OMP_NUM_THREADS" "14"
            |> set_runtime_vars
  in
    eval ~context @@ glue

  let exec_par conf args tile num_core =
    Printf.printf "Executing in parallel with %d cores\n" num_core;
    let context = Context.(create ~cwd:(Path "../c_bench") ()) in
    (* If dispose is not called at some point, we'll have a resource leak (too
     * many files opened) *)
    Gc.finalise Context.dispose context;
  let  glue = glue_all conf args tile
            |> set_ld_library
            |> set_env "OMP_NUM_THREADS" (string_of_int num_core)
  in
    eval ~context @@ glue

end

module Prc = Shexp_process
open Shexp_process.Infix
open Batteries

let build_marker mark s =
  let reg = Str.regexp @@ ".*" ^ mark ^ ".*" in
  match Str.search_forward reg s 0 with
  | exception _ -> false
  | _ -> true

type replacing = Before | During | After

exception MarkNotFound

let fold_replace begin_marker end_marker lines_to_insert (lines, replacing_state) line =
  let is_begin = build_marker begin_marker
  and is_end = build_marker end_marker in
  match replacing_state with
  | Before ->
    if is_begin line then (lines, During)
    else (line::lines, Before)
  | During ->
    if is_end line then (List.rev_append lines_to_insert lines, After)
    else (lines, During)
  | After -> (line::lines, After)

let fold_replace_shxp begin_marker end_marker lines_to_insert =
  let fold state line = Prc.return @@ fold_replace begin_marker
      end_marker lines_to_insert state line in
  Prc.fold_lines ~init:([], Before) ~f:fold
  >>= function
        (lines, After) -> Prc.return @@ String.concat "\n" @@ List.rev lines
      | (_, _) -> raise MarkNotFound

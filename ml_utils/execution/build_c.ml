open Shexp_process
open Batteries
open Bench_types
open Config

let common_files = 
  "buffer.c timing.c main.c mem_utils.c gen/handle_counters.c"
  |> String.split_on_char ' '

let blis_link blis_location arch_name =
  blis_location ^ "/lib/" ^ arch_name ^ "/libblis.a"

let bench_sources  blis_location arch_name =
  let common_src: type a. a bench -> string = function
      Gen | Blis | Mkl -> "matrix_utils.c"
    | Conv | Rui | OneDNN -> "conv_utils.c" in
  let string_of_bench: type a. a bench -> string = function
    | Gen -> "gen/gen_matmul.c"
    | Blis -> blis_link blis_location arch_name
    | Mkl -> ""
    | Conv -> "gen/gen_conv.c"
    | Rui -> "rui_conv/bbbb"
    | OneDNN -> "onednn/oneDNN_conv.c"
  in fun benches ->
    (List.first benches |> common_src)
    :: (List.map string_of_bench benches)

let common_opts =[ "-g"; "-Wall"; "-fopenmp"; "-Wno-unused-function";
                   "-march=native";(*"-xcore-avx512";*)"-fno-align-loops"; ] 

let compile_options blis_location arch_name benches =
  let opt_of_bench (type a) (b: a bench) = match b with
    | Blis -> Some ("-I" ^ blis_location ^ "/include/" ^ arch_name)
    | Mkl -> Some "-mkl=sequential"
    | Gen | Conv | Rui | OneDNN -> None in
  common_opts @ List.filter_map opt_of_bench benches

let common_lib = ["-L/usr/lib";  "-lm"; "-lpthread"; "-lpapi"]

let link_option :'a bench list -> string list =
  let lib_of_bench: type a. a bench -> string option = function
    | Mkl -> Some "-lmkl_rt"
    | OneDNN -> Some "-ldnnl"
    | Gen | Blis | Conv | Rui -> None in
  fun benches ->
    List.append
      common_lib
      (List.filter_map lib_of_bench benches)

let opt_str = function
  | O0 -> "-O0"
  | O1 -> "-O1"
  | O2 -> "-O2"
  | O3 -> "-O3"

let build_command compiler opt_level include_dirs special_libs blis_location arch_name benches =
  let compiler = match compiler with Gcc -> "gcc"
                                   | Cc -> "cc"
                                   | Icc -> "icc"
                                   | Clang -> "clang"
  and libs = List.concat_map (fun s -> ["-L";  s]) special_libs
  and includes = List.concat_map(fun s -> ["-I"; s]) include_dirs
  and compile_options =  compile_options blis_location arch_name benches in
  let args = List.flatten [[opt_str opt_level]; compile_options; includes;
                           common_files; bench_sources blis_location arch_name benches;
                           libs; link_option benches; ["-o"; "mm_bench.x"] ] in
  compiler, args

let print_build_command compiler ?(opt_level=O3) include_dirs special_libs blis_location arch_name benches =
        build_command  compiler opt_level include_dirs  special_libs blis_location arch_name benches
  |> fun (compiler, args) -> String.concat " " @@ compiler::args
  |> print_endline

let compile compiler ?(opt_level=O3) include_dirs  special_libs blis_location  arch_name benches =
  let compiler, args = build_command  compiler opt_level include_dirs  special_libs blis_location arch_name benches in
  run compiler args

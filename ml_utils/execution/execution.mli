open Common
include module type of Modules_decl

module Bench(A:Arch.Arch_t): sig
  val gen_code: 'a Config.layout -> 'a Config.args -> 'a Codegen.Tile(A).tile_scheme
    -> string
  val gen_file: 'a Config.layout -> 'a Config.args -> 'a Codegen.Tile(A).tile_scheme
    -> string -> unit
  val exec: 'a Config.t -> 'a Config.args -> 'a Codegen.Tile(A).tile_scheme
    -> 'a Config.results
  val exec_par: 'a Config.t -> 'a Config.args
    -> 'a Codegen.Tile(A).tile_scheme -> int
    -> 'a Config.results
end

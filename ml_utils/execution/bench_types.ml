type conv = Conv_w
type mm   = MM_w

type conv_or_mm = Conv | MM

type compiler =  Gcc | Cc | Icc | Clang [@@deriving show {with_path = false}]
type opt_level =  O0 | O1 | O2 | O3 [@@deriving show {with_path = false}]

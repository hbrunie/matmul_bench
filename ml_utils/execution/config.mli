open Bench_types

type _ bench = Blis: mm bench
             | Mkl:  mm bench
             | Gen:  mm bench
             | Conv: conv bench
             | Rui:  conv bench
             | OneDNN:  conv bench

val show_bench: 'a bench -> string

type conv_layout_in = XYC | YXC | XCY | CYX [@@deriving show {with_path=false}]
type conv_layout_par = HWCF | WHCF | FCHW [@@deriving show {with_path=false}]
type conv_layout_out = YXF | XYF | FYX [@@deriving show {with_path=false}]

type _ layout = MM_layout: mm layout
              | Conv_layout: conv_layout_out * conv_layout_in * conv_layout_par -> conv layout

val default_layout: 'a bench -> 'a layout 

type _ args =
    MM_args: (int * int * int) -> mm args
  (*| Conv_args: (int * int * int * int * int * int * (int * int) option) -> conv args*)
  | Conv_args: {x: int; w : int; y : int; h: int ;c: int; f: int;
                englobing_input: (int * int * int * int * int * int) option;
                englobing_params: (int * int * int * int * int * int) option;
                englobing_output: (int * int * int * int * int * int) option;
                strides:(int * int) option} -> conv args

val mm_args: int -> int -> int -> mm args
val conv_args: ?englobing_output : int * int * int * int * int * int  ->
  ?englobing_input: int * int * int * int * int * int  ->
  ?englobing_params: int * int * int * int * int * int  ->
  int -> int -> int -> int -> int -> int -> conv args

val conv_args_stride: ?englobing_output: int * int * int * int * int * int ->
  ?englobing_input: int * int * int * int * int * int  ->
  ?englobing_params: int * int * int * int * int * int  ->
  int -> int -> int -> int -> int -> int -> int -> int -> conv args

type error = EXACT_EPS of float | EXACT | EPSILON of float | NO_CHECK

type 'a conf = {
  (* None => Don't recompile or regenerate anything.
   * Some (regen_kern, regen_framework, compiler, optimization_level =>
   * if regen_kern is true, regenerate kernel - gen_matmul.
   * if regen_framework is true, regenerate kernel the rest of the code -
   * params, size and all.
   * Compile with compiler and optimization_level *)
  generation:(bool * bool * compiler * opt_level) option;
  blis_lib: string;
  error: error;
  benches: ('a bench * 'a layout) list;
  num_rep: int; counters: Counter.t list }

type 'a t =
    MM_conf: mm conf -> mm t
  | Conv_conf: conv conf -> conv t

val build_mm_config: ?num_rep:int -> ?error:error -> ?blis_lib:string ->
  (bool * bool * compiler * opt_level) option ->  mm bench list
  -> Counter.t list -> mm t

val build_conv_config: ?num_rep:int -> ?error:error -> ?blis_lib:string ->
  (bool * bool * compiler * opt_level) option ->  conv bench list
  -> Counter.t list -> conv t

val build_conv_config_with_layout: ?num_rep:int -> ?error:error -> ?blis_lib:string ->
  (bool * bool * compiler * opt_level) option ->  (conv bench * conv layout) list
  -> Counter.t list -> conv t

val to_conf: 'a t -> 'a conf

val is_mkl: 'a bench -> bool
val is_blis: 'a bench -> bool
val is_gen: 'a bench -> bool
val is_conv: 'a bench -> bool

type 'a results
val results: 'a t -> string -> 'a results
val show_results: 'a results -> string
val select_bench: 'a bench -> 'a results -> (Counter.t * int) list
val blis_ctrs: mm results -> (Counter.t * int) list
val mkl_ctrs: mm results -> (Counter.t * int) list
val gen_ctrs: mm results -> (Counter.t * int) list
val select_bench_counter: 'a bench -> Counter.t -> 'a results ->  int
val blis_cycles: mm results ->  int
val mkl_cycles: mm results ->  int
val gen_cycles: mm results ->  int
val conv_cycles: conv results -> int
val rui_cycles: conv results -> int

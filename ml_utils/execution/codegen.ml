open Batteries
open Common
open Arch
module TL = Tensor_loops



type 'a params = { error: Config.error;
                   benches: ('a Config.bench * 'a Config.layout) list;
                   num_rep: int;
                   counters: Counter.t list}

let build_params num_rep benches error counters =
  {benches; num_rep; counters = List.sort_uniq Counter.compare counters; error;}

let gen_params (module Arch: Arch_t) {num_rep; benches; error; counters; } =
  let error_str = match error with 
      EXACT_EPS eps -> Printf.sprintf "#define ERROR_EXACT\n#define ERROR_EPS\n#define EPSILON %f" eps
    | EPSILON eps -> Printf.sprintf "#define ERROR_EPS\n#define EPSILON %f" eps
    | EXACT -> "#define ERROR_EXACT"
    | NO_CHECK -> "" in
  let config_str (type a) (b:a Config.bench) = let open Config in
    match b with
    | Blis -> "#define BLIS"
    | Mkl -> "#define MKL"
    | Gen ->  "#define GEN"
    | Conv -> "#define USE_CONV\n#define GEN"
    | Rui -> "#define USE_CONV\n#define RUI"
    | OneDNN -> "#define USE_CONV\n#define ONEDNN" in
  let bench_str = String.concat "\n" (List.map (config_str % fst) benches) in
  let layout_vars (type a) ((b, l) :a Config.bench * a Config.layout) = let open Config in
    let def_vars prefix lay_out lay_in lay_par =
      let lout = show_conv_layout_out lay_out
      and lin =  show_conv_layout_in lay_in
      and lpar =  show_conv_layout_par lay_par in
      Printf.sprintf
        "#define %s_LAYOUT_OUT %s\n#define %s_LAYOUT_IN %s\n#define %s_LAYOUT_PAR %s\n"
        prefix lout prefix lin prefix lpar in
    match b, l with
    | _, MM_layout -> None
    | Conv, Conv_layout (lay_out, lay_in, lay_par) ->
      Some (def_vars "GEN" lay_out lay_in lay_par)
    | Rui, Conv_layout (lay_out, lay_in, lay_par) ->
      Some (def_vars "RUI" lay_out lay_in lay_par)
    | OneDNN, Conv_layout (lay_out, lay_in, lay_par) ->
      Some (def_vars "DNN" lay_out lay_in lay_par) in
  let layout_str = String.concat "\n" (List.filter_map layout_vars benches) in
  let enumerate = let rec enum i = function
      | h::t -> (i, h)::(enum (i + 1) t)
      | [] -> []
    in enum 0 in
  let counter_str =
    enumerate counters
    |> List.map (fun (i, ctr) -> Printf.sprintf "#define %s %d"
                    (Counter.show ctr) i)
    |> String.concat "\n" in
  let num_counter_str =
    let n_ctr = List.length counters in
    Printf.sprintf "#define N_CTR %d" n_ctr in
  let rep_str = Printf.sprintf "\n#define NUM_REP (%d)\n" num_rep
  and print_res = "#undef PRINT_RESULTS"
  and allocator = "#undef USE_CUSTOM_ALLOC"
  and architecture = "#define " ^ Arch.arch_type in 
  String.concat "\n" [ architecture; bench_str; layout_str; error_str; counter_str;
                       num_counter_str; rep_str; allocator; print_res ]

let from_config conf = let open Config in
  let {benches; num_rep; counters; error;_} = Config.to_conf conf in
  build_params num_rep benches error counters

let from_config_with_layout conf benches = let open Config in
  let { num_rep; counters; error;_} = Config.to_conf conf in
  build_params num_rep benches error counters

let gen_events_name counters =
  let counters = List.sort_uniq Counter.compare counters in
  let str_from_counter c =
    Printf.sprintf "#ifdef %s\n\t\"%s\",\n#endif" (Counter.show c) (Counter.counter_name c) in
  String.concat "\n" (List.map str_from_counter counters)

let gen_counter_print  counters =
  let counters = List.sort_uniq Counter.compare counters in
  let str_from_counter c =
    let ctr_s = Counter.show c in
    Printf.sprintf
      "#ifdef %s\n\tprintf(\"%%lld\\n\", ctrs[%s]);\n#endif" ctr_s ctr_s
  in
  String.concat "\n" (List.map str_from_counter counters)


(* Matmul gen *)

let dim_gen = TL.Ids.Dim.fresh_gen ()

module Conv_dims = struct
    let x_dim = dim_gen ~name:"x" ()
    let w_dim = dim_gen ~name:"w" ()
    let y_dim = dim_gen ~name:"y" ()
    let h_dim = dim_gen ~name:"h" ()
    let c_dim = dim_gen ~name:"c" ()
    let f_dim = dim_gen ~name:"f" ()
end

module MM_dims = struct
    let i_dim = dim_gen ~name:"i" ()
    let j_dim = dim_gen ~name:"j" ()
    let k_dim = dim_gen ~name:"k" ()
end

module Tile(A: Arch_t) = struct

  module MM = struct
    open TL
    include Tensor_tile(A.M)
    include MM_build(struct
        include MM_dims

        let a = Tensor.make ~name:"A" [|k_dim; i_dim |]
        let b = Tensor.make ~name:"B" [|j_dim; k_dim |]
        let c = Tensor.make ~name:"C" [|j_dim; i_dim |]
      end)

    let build_dim_sizes i j k =
      [i_dim, i; j_dim, j; k_dim, k]
  end


  module Conv = struct
    include Conv_dims
    open TL
    include Tensor_tile(A.M)
    open Config
    open TL
    open Tensor

    let input_layout =
      let c = single c_dim
      and yh = join_dims y_dim h_dim
      and xw =join_dims x_dim w_dim in
      function XYC -> [| c; yh; xw|]
             | YXC -> [| c; xw; yh|]
             | XCY -> [| yh; c; xw|]
             | CYX -> [| xw; yh; c|]

    let input_layout_stride strx stry =
      let c = single c_dim
      and yh = join_dims_stride y_dim h_dim stry
      and xw = join_dims_stride x_dim w_dim strx in
      function XYC -> [| c; yh; xw|]
             | YXC -> [| c; xw; yh|]
             | XCY -> [| yh; c; xw|]
             | CYX -> [| xw; yh; c|]

    let input_strides_from_layout stry strx (_,c,y,x,h,w)  =
      let c_s = single c_dim
      and yh = join_dims_stride y_dim h_dim stry
      and xw = join_dims_stride x_dim w_dim strx in
      function
        XYC -> [c_s,  1; yh, c; xw, c * (stry * y + h - 1)]
      | YXC -> [c_s, 1; xw, c; yh, c * (strx * x + w - 1)]
      | XCY -> [yh, 1; c_s, (stry * y + h - 1); xw, c * (stry * y + h - 1)]
      | CYX -> [xw, 1; yh, (strx * x + w - 1); c_s, (strx * x + w - 1) * (stry * y + h - 1)]

    let params_layout = function
      | WHCF -> [|f_dim; c_dim; h_dim; w_dim;|]
      | HWCF -> [|f_dim; c_dim; w_dim; h_dim;|]
      | FCHW -> [|w_dim; h_dim; c_dim; f_dim;|]

    let params_strides_from_layout (f,c,_,_,h,w)  = function
      | WHCF -> [f_dim, 1; c_dim, f; h_dim, c * f; w_dim, h * c * f;]
      | HWCF -> [f_dim, 1; c_dim, f; w_dim, c * f; h_dim, w * c * f;]
      | FCHW -> [w_dim, 1; h_dim, w; c_dim, w * h; f_dim, w * h * c;]

    let output_layout = function
        YXF -> [|f_dim; x_dim; y_dim; |]
      | XYF -> [|f_dim; y_dim; x_dim; |]
      | FYX -> [|x_dim; y_dim; f_dim; |]

    let output_strides_from_layout (f,_,y,x,_,_) = function
        YXF -> [f_dim, 1; x_dim, f; y_dim, f * x; ]
      | XYF -> [f_dim, 1; y_dim, f; x_dim, f * y; ]
      | FYX -> [x_dim, 1; y_dim, x; f_dim, x * y; ]

    module type Conv_sign = sig
    include Sign.Conv_args_t
    val gen_code: ?dim_sizes:((Ids.Dim.t * int) list) -> tile_scheme -> string
    end

    let build_conv_module out in_ params: (module Conv_sign) = 
      let input = Tensor.make_join ~name:"input" @@ input_layout in_
      and params = Tensor.make ~name:"params" @@ params_layout params
      and output = Tensor.make ~name:"output" @@ output_layout out in
      (module Conv_build( struct
        include Conv_dims
        let input = input let params = params let output = output
      end))


    let build_conv_module_stride out in_ params strx stry: (module Conv_sign) = 
      let input = Tensor.make_join ~name:"input" @@ input_layout_stride strx stry in_
      and params = Tensor.make ~name:"params" @@ params_layout params
      and output = Tensor.make ~name:"output" @@ output_layout out in
      (module Conv_build( struct
        include Conv_dims
        let input = input let params = params let output = output
      end))

    let build_conv_module_explicit_strides
        output_sizes input_sizes param_sizes out in_ params strx stry: (module Conv_sign) = 
      let open Exprs in
      let strides_opt f layout sizes = 
        sizes
        |> Option.map @@ fun sizes ->
        f sizes  layout
        |> List.map (Utils.map_snd (fun c -> Expr.const c)) in
      let dims_in_strides = strides_opt (input_strides_from_layout strx stry) in_ input_sizes
      and dims_par_strides = strides_opt params_strides_from_layout params param_sizes
      and dims_out_strides = strides_opt output_strides_from_layout out output_sizes in
      let from_stride_opt name layout =
        Option.map_default
          (fun strides -> Tensor.make ~strides ~name layout) (Tensor.make ~name layout) in
      let input = Option.map_default
          (fun strides ->
             Tensor.make_join ~strides:strides  ~name:"input"
             @@ input_layout_stride strx stry in_)
          (Tensor.make_join ~name:"input" @@ input_layout_stride strx stry in_)
          dims_in_strides
      and params = from_stride_opt "params" (params_layout params) dims_par_strides
      and output = from_stride_opt "output" (output_layout out) dims_out_strides in
      (module Conv_build( struct
           include Conv_dims
           let input = input let params = params let output = output
         end))

    let build_dim_sizes x w y h c f =
      [ x_dim, x; w_dim, w; y_dim, y; h_dim, h;  c_dim, c; f_dim, f]
  end



  type _ tile_scheme =
    | MM_tile: MM.tile_scheme -> Bench_types.mm tile_scheme
    | Conv_tile: Conv.tile_scheme -> Bench_types.conv tile_scheme

  let gen_code: type a. a Config.layout -> a Config.args -> a tile_scheme  -> string =
    let open Config in
    fun layout arg scheme -> match layout, arg, scheme with
      | _, MM_args (i,j,k), MM_tile t ->
        let dim_sizes = MM.build_dim_sizes i j k in
        MM.gen_code ~dim_sizes t
      | Conv_layout (out, in_, params),
        Conv_args {x; w; y; h; c; f; englobing_input; englobing_output ; englobing_params; strides }, Conv_tile t ->
        match  strides with
        | Some (str_x, str_y) ->
        let module Conv_l =
          (val Conv.build_conv_module_explicit_strides 
              englobing_output englobing_input englobing_params
              out in_ params str_x str_y ) in
        let dim_sizes = Conv.build_dim_sizes x w y h c f in
        Conv_l.gen_code ~dim_sizes t
        |  None ->
        let module Conv_l =
          (val Conv.build_conv_module_explicit_strides
              englobing_output englobing_input englobing_params
              out in_ params  1 1 ) in
        let dim_sizes = Conv.build_dim_sizes x w y h c f in
        Conv_l.gen_code ~dim_sizes t

end

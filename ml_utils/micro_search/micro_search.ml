open Batteries
open Common
open Execution
open Bench_types 
open Search

open Conv_search_type
open Conv_spec

module S(Ainfo : Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  module Tile = Tile
  module Pred_conv = Arch.Pred_conv(Ainfo.A)

  open Streaming

  type select_mode = Rand of int
                   | Sorted of int
                   | Sorted_mixed of int
                   | All

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  let gen_schemes perms size num_candidates =
    gen_all perms size
    |> (match num_candidates with
          Sorted num_candidates -> sort_and_select_volume num_candidates
        | Sorted_mixed num_candidates -> select_mixed 50 num_candidates 40
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_scheme

  let exec_scheme counters_list (f, c, x, y, h, w) stride scheme =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:1
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
    let args = match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy
    in
     scheme,
     let res = (B.exec conf_gen args
                  (Tile.Conv_tile scheme)) in
     List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
       counters_list



  let sort_output stream =
    Stream.into Sink.list stream
    |> Utils.sort_by (List.assoc Counter.CYCLES % snd)

  let format_first_line counter_list =
    let open Counter in
    List.map (function CYCLES -> "perf"
                     | c -> show c)
      counter_list
    |> String.concat ","

  let format_tile tile =
    Tile.Conv.tile_to_string tile
    |>  String.filter (function '\n' -> false | _ -> true)
    |> fun s -> "\"" ^ s ^ "\""

  let format_flow_line (f,c,y,x,h,w) counter_list res =
    let open Counter in
    List.map (function
        | CYCLES ->
          (List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
           |> Printf.sprintf "%.2f")
        | c -> List.assoc c res
               |> string_of_int
      ) counter_list
    |> String.concat "," 

  (* Write results to file *)
  let res_write name sizes counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list ^ ",scheme" in
      IO.write_line f first_line; f
    and push file (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s,%s\n" line (format_tile tile);
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow sizes counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.printf "%s\n%s\n" (Tile.Conv.tile_to_string tile) line
    and stop () = () in
    Sink.make ~init ~push ~stop ()

  let exec_with_perm counter_list perm num_candidate conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes (Some perm) adapted_size (Sorted num_candidate)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
           )
      ) conv_spec

  let exec_all counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size All
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_rand counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size (Rand 200)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_new_metric_def_perms counter_list num_candidates conv_spec =
    List.iter
      (fun (perm, {name; adapted_size; stride;_}) ->
         gen_schemes (Some perm) adapted_size (Sorted_mixed num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec
  let exec_new_metric counter_list num_candidates conv_spec =
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size (Sorted_mixed num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_and_sort_results counter_list num_candidates conv_spec =
    List.map
      (fun {name; adapted_size; stride;_} ->
         name,
         stride,
         adapted_size,
         gen_schemes None adapted_size (Sorted num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> sort_output
      ) conv_spec
end


(* Warning : change that *)
module S_avx512 = S(Arch_info.Skylake_info)
let zip_pair l1 l2 = List.map2 (fun x y -> x, y) l1 l2

let () = S_avx512.exec_new_metric  Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]
    50 Conv_sizes.all_spec

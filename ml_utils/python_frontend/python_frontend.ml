open Python_lib
open Python_lib.Let_syntax
open Common
open Search
module CST = Conv_search_type

type return_type =
 ((CST.dim * int) list * (CST.dim * int) list * int * string) list list [@@deriving python]

module S(Ainfo : CST.Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  open Execution
  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  module Tile = Tile
  module Pred_conv = Arch.Pred_conv(Ainfo.A)

  open Streaming
  type select_mode = Rand of int | Sorted of int | All

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  let gen_schemes_tvm_style perm ((_,c,_,_,_,_) as size) num_candidates =
    gen_all perm size
    |> (match num_candidates with
          Sorted num_candidates -> sort_and_select c num_candidates
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_tvm

  let gen_code englobing_output englobing_input englobing_params (f',c',y',x',h',w') stride scheme =
    let args = match stride with None -> Config.conv_args ~englobing_output ~englobing_input ~englobing_params
                                           x' w' y' h' c' f'
                               |Some (sx, sy) ->
                                 Config.conv_args_stride ~englobing_output ~englobing_input ~englobing_params
                                   x' w' y' h' c' f' sx sy in
    let layout = Config.Conv_layout (XYF, XYC, WHCF) in
    B.gen_code layout args (Tile.Conv_tile scheme)

  let gen_tvm_schemes num_candidates ((f,c,_,x,h,w) as adapted_size) stride =
    gen_schemes_tvm_style None adapted_size (Sorted num_candidates)
    |> Stream.map (
      List.map (fun (l_out, l_tens, size_y, scheme) ->
          let size d =List.assoc d l_tens in
          let sizes = Conv_search_type.(size F, size C, size Y, size X, size H, size W) in
          l_out, l_tens, size_y, gen_code (f,c,size_y,x,h,w) adapted_size adapted_size sizes stride scheme)
    )
    |> Stream.into Sink.list
     |> python_of_return_type

end


let arch_of_string : string -> (module CST.Arch_info_t) =
  function
  | "skylake" -> (module Arch_info.Skylake_info)
  | "xeon" -> (module Arch_info.Xeon)
  | "broadwell" -> (module Arch_info.Broadwell)
  | "E52630" -> (module Arch_info.E52630)
  | "silver" -> (module Arch_info.Silver_info)
  | _-> failwith "Unknown module"


let ret_type =
  Defunc.Of_python.create
    ~type_name:"list_schemes"
    ~conv:return_type_of_python

let conv_size_type =
  Defunc.Of_python.create
    ~type_name:"sizes"
    ~conv:[%of_python: int * int * int * int * int * int]

let gen_tvm_schemes (module A:CST.Arch_info_t) num_cand size stride =
  let open S(A) in
  gen_tvm_schemes num_cand size stride

let gen_py_scheme =
  let%map_open arch = positional ~docstring:"" "num_candidate" string
  and num_cand = positional ~docstring:"" "num_candidate" int
  and f = positional ~docstring:"" "f" int
  and c = positional ~docstring:"" "c" int
  and y = positional ~docstring:"" "y" int
  and x = positional ~docstring:"" "x" int
  and h = positional ~docstring:"" "h" int
  and w = positional ~docstring:"" "w" int
  and stride = positional ~docstring:"" "stride" int in
  gen_tvm_schemes (arch_of_string arch) num_cand (f,c,y,x,h,w) (Some (stride, stride))

let () =
  if not (Py.is_initialized ()) then Py.initialize ();
  let mod_ = Py_module.create "ttile" in
  Py_module.set mod_ "gen_schemes"
    ~docstring:"Generate schemes from arch and convolution description" gen_py_scheme

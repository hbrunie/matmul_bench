open Batteries
open Common
open Utils
open Execution
open Bench_types 
open Search

open Conv_search_type
open Conv_spec

module S(Ainfo : Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  module Tile = Tile
  module Pred_conv = Arch.Pred_conv(Ainfo.A)

  open Streaming
  type select_mode = Rand of int | Sorted of int | All

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  let gen_schemes_tvm_style perm ((_,c,_,_,_,_) as size) num_candidates =
    gen_all perm size
    |> (match num_candidates with
          Sorted num_candidates -> sort_and_select c num_candidates
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_tvm

  let gen_schemes perm ((_,c,_,_,_,_) as size) num_candidates =
    gen_all perm size
    |> (match num_candidates with
          Sorted num_candidates -> sort_and_select c num_candidates
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_scheme


  (* Added by Guillaume - did not find a documentation for a Stream.mapi (to replace when calling "exec_scheme")
    TODO: make it better (in term of programming taste)
   *)
  let version_num = ref 0 (* Berk! *)

  let exec_scheme counters_list (f, c, x, y, h, w) stride scheme  =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:1
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
    let args = match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy
    in

    (* Generate a file - added by Guillaume *)
    let layout = Config.Conv_layout (XYF, XYC, WHCF) in
    B.gen_file layout args (Tile.Conv_tile scheme) (Printf.sprintf "_vnum%d" !version_num) ;
    version_num := !version_num + 1;
    (* End - added by Guillaume *)


     scheme,
     let res = (B.exec conf_gen args
                  (Tile.Conv_tile scheme)) in
     List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
       counters_list


  let format_first_line counter_list =
    let open Counter in
    List.map (function CYCLES -> "perf"
                     | c -> show c)
      counter_list
    |> String.concat ", "

  let sort_output stream =
    Stream.into Sink.list stream
    |> List.sort
      (fun (_, res1) (_, res2) ->
         let get_cycles res = List.assoc Counter.CYCLES res in
         Int.compare (get_cycles res1) (get_cycles res2))

  let format_flow_line (f,c,y,x,h,w) counter_list res =
    let open Counter in
    List.map (function
        | CYCLES ->
          (List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
           |> Printf.sprintf "%.2f")
        | c -> List.assoc c res
               |> string_of_int
      ) counter_list
    |> String.concat "," 

  (* Write results to file *)
  let res_write name sizes counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list in
      IO.write_line f first_line; f
    and push file (_, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s\n" line;
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()

  (* Write tile schemes to file *)
  let tile_write name  =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
                    1, File.open_out (loop (A.arch_name ^ "_" ^ name ^ "_tiles" ^
                                       ".log") )
    and push (count, file) (tile, _) =
      Printf.fprintf file "%d : %s\n" count (Tile.Conv.tile_to_string tile);
      IO.flush file;
      count + 1, file
    and stop (_, f) = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow sizes counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.printf "%s\n%s\n" (Tile.Conv.tile_to_string tile) line
    and stop () = () in
    Sink.make ~init ~push ~stop ()

  let exec_all counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size All
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            <& tile_write name)
      ) conv_spec

  let exec_rand num_candidates counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size (Rand num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_rand_with_default_perm num_candidates counter_list conv_spec_perms =
    let open Sink in
    List.iter
      (fun ({name; adapted_size; stride;_}, perm) ->
         gen_schemes (Some perm) adapted_size (Rand num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec_perms

  let exec_and_sort_results counter_list num_candidates conv_spec =
    List.map
      (fun {name; adapted_size; stride;_} ->
         name,
         stride,
         adapted_size,
         gen_schemes None adapted_size (Sorted num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> sort_output
      ) conv_spec
end

let search_all (module Ainfo: Arch_info_t) names_sizes_strides =
  let open S(Ainfo) in
  let open Counter in
  let build_args (f,c,y,x,h,w) stride =
     match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy in
  exec_and_sort_results [CYCLES] 50 names_sizes_strides
  |> List.map
    (fun (name, stride, ((f,c,y,x,h,w) as sizes), l_res) ->
       name,
       List.map ( fun (scheme, res) ->
           Tile.Conv.tile_to_string scheme,
           Tile.gen_code Config.(default_layout Conv) (build_args sizes stride)
             (Tile.Conv_tile scheme),
           List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
       ) l_res
    )

let find_best_and_log (ainfo: (module Arch_info_t)) filename names_sizes_strides =
  let list_bests = search_all ainfo names_sizes_strides in
  let best_of_bests = List.map (map_snd List.hd) list_bests in
  File.with_file_out filename (fun f ->
      Printf.fprintf f "name, perf, best scheme";
      List.iter (fun (name, (tile_scheme, _, perf)) ->
          Printf.fprintf f "%s, %.2f, %s\n" name perf tile_scheme 
        )
    ) best_of_bests;
  List.iter (fun (name, (tile_scheme, tile_code, perf)) ->
      let filename = filename ^ "_" ^ name ^ "_code.c"  in
      File.with_file_out filename (fun f ->
          Printf.fprintf f "//%.2f, %s\n\n" perf tile_scheme ;
          Printf.fprintf f "//%s\n" tile_code ;
        )
    ) best_of_bests;
  List.iter (fun (name, l_res) ->
      let filename = filename ^ "_" ^ name in
      File.with_file_out filename (fun f ->
          List.iter (fun (tile_scheme, _, perf) ->
              Printf.fprintf f "%.2f, %s\n" perf tile_scheme 
            ) l_res
        )
    ) list_bests

let names_sizes_strides = let open Conv_sizes in
  Utils.map3 (fun name size stride -> name, size, stride)
    all_names all_sizes all_strides

let zip_pair l1 l2 = List.map2 (fun x y -> x, y) l1 l2

module S_avx512 = S(Search.Arch_info.Xeon)

(* Example rand
let () = S_avx512.exec_rand_with_default_perm 200
    (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    Conv_sizes.all_spec
*)
(* Example mobileNet1 only *)
let () = S_avx512.exec_rand 200
    (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    [Conv_sizes.mobilNet_1_spec]

(* Example mobileNet1, mobileNet2 only *)
let () = S_avx512.exec_rand 200
    (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    Conv_sizes.[mobilNet_1_spec; mobilNet_2_spec]

(* Example mobileNet1, mobileNet2 only with default perm *)
let () = S_avx512.exec_rand_with_default_perm 200
    (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    Conv_sizes.(
      Auguste_perms.[mobilNet_1_spec, mbnet_1; mobilNet_2_spec, mbnet_2])

let () = S_avx512.exec_rand_with_default_perm 200
    (Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2])
    (zip_pair Conv_sizes.all_spec Auguste_perms.all_perms)

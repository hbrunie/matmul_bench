open Batteries

let remove_map p =
  let rec loop acc = function
    | [] -> failwith "not found"
    | h::t  -> match p h with Some ret -> ret, List.rev_append acc t | None -> loop (h::acc) t in loop []

let insert_anywhere e l =
  let rec loop = function
    | h::t -> (e :: h :: t) :: (List.map (List.cons h) (loop t))
    | [] -> [[e]] in
  loop l

let take_whilei p l =
  let rec loop acc i = function
    | h::t -> if (p h i) then loop (h::acc) (i + 1) t
      else List.rev acc
    | [] -> List.rev acc in
  loop [] 0 l

let cartesian ll =
  let rec loop acc = function
    | [] -> acc
    | h::t ->
      let acc' = List.concat_map (fun x -> List.map (fun t -> t @ [x]) acc) h in
      loop acc' t in
  match ll with [] -> []
              | h::t -> loop (List.map (fun x -> [x]) h) t

let range a b = List.init (b - a + 1) ((+) a) 

let compare_by ?(cmp=Stdlib.compare) metric a1 a2 =
  cmp (metric a1) (metric a2)

let sort_by ?(cmp=Stdlib.compare) metric =
  List.sort (compare_by ~cmp metric)

let bimap f g (x, y) = f x, g y

let map_fst f = bimap f Fun.id

let map_snd f = bimap Fun.id f

let rec map3 f l1 l2 l3 = match l1, l2, l3 with
  | h1 :: t1, h2 :: t2, h3 :: t3 -> (f h1 h2 h3)::(map3 f t1 t2 t3)
  | [], [], [] -> []
  | _ -> failwith "map3 called with heteregeneous size lists"

let rec map4 f l1 l2 l3 l4 = match l1, l2, l3, l4 with
  | h1 :: t1, h2 :: t2, h3 :: t3,  h4 :: t4 -> (f h1 h2 h3 h4)::(map4 f t1 t2 t3 t4)
  | [], [], [], [] -> []
  | _ -> failwith "map4 called with heteregeneous size lists"

type ('a, 'b) either = ('a, 'b) Batteries.Either.t = Left of 'a | Right of 'b
                                                                           [@@deriving
                                                                             show]

(* Way too complicated for such a simple thing.
 * Take a list of list
 * and returns the list of (list of first elements, list of second element, list of third...) *)
let zip_all l =
  let take_heads ll =
    List.fold_right
      (fun next acc ->
         Option.bind acc
           (fun (hs, ts) -> match next with
                [] -> None
              | h'::t' -> Some (h'::hs, t'::ts)
           ))
      ll (Some ([], [])) in
  let rec loop acc ll =
    match take_heads ll with None -> List.rev acc
                           | Some (heads, tails) -> loop (heads::acc) tails in
  loop [] l


let tee f x = f x; x

let protect f filename =
  let rec loop fn = if Sys.file_exists fn then
     loop (fn ^ "x")
    else fn in
  f (loop filename)

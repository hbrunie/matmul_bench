open Batteries

let range_exc n m = if m < n then [] else List.init (m - n) (fun i -> n + i)
let range_incl n m = range_exc n (m + 1)

let decompose n k = 
  let rec loop l n k = match k, n with
    | 0, 1 -> [l]
    | 1, _ -> [n::l]
    | 0, _ -> []
    | _, _ -> range_incl 1 n
              |> List.concat_map
                (fun i -> if n mod i <> 0 then []
                  else loop (i::l) (n / i) (k - 1)
                )
  in loop [] n k

let all_divisor n =
  List.init n  ((+) 1)
|> List.filter (fun k -> n mod k = 0)

(* Find u, v such that u * a + v * b = n if it exists *)
let pair_dec a b n =
  let rec loop a b k  =
    if (n - k * a < b) then None else 
    if (n - k * a) mod b = 0
    then Some (k, (n - k * a) / b)
    else loop a b (k + 1) in
  if a < b then loop a b 1
  else Option.map (fun (x, y) -> y, x) @@ loop b a 1

open Batteries
open Angstrom

let is_alpha = function 'a' .. 'z' | 'A' .. 'Z' -> true | _ -> false

let alpha = satisfy is_alpha
let is_whitespace = function
  | '\x20' | '\x0a' | '\x0d' | '\x09' -> true
  | _ -> false

let whitespace = many (satisfy is_whitespace)

let num_char = satisfy (function '0' .. '9' -> true | _ -> false)

let enclose_ws value =
whitespace *> value <* whitespace

let integer =
  let+ chars = many1 num_char in
  int_of_string @@ String.implode chars

let float =
  let* pre = many1 num_char in
  let* point = char '.'  in
  let+ post = many num_char in
  pre @ [point] @ post
  |> String.implode
  |> float_of_string

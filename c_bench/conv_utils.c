#include "conv_utils.h"

void init_zero(M_TYPE * tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = 0.;
	}
}

void init_identity(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			if (i == j) {tab[i * rsize + j] = 1.;}
			else {tab[i * rsize + j] = 0.;}
		}
	}
}


void init_bounded(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = (i *13 + j * 12) % bound;
			tab[i * rsize + j] = ((M_TYPE) val);
		}
	}
}

void init_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = rand() % bound;
			tab[i * rsize + j] = ((M_TYPE) val);
		}
	}
}

void init_signed_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = rand() % bound;
			tab[i * rsize + j] = (rand() & 1)? ((M_TYPE) val): ((M_TYPE) -val);
		}
	}
}

void init_rand_zero_one(M_TYPE * tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
                  tab[i] = (M_TYPE)rand() / (M_TYPE)RAND_MAX;
	}
}

void init_par(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = ((M_TYPE) i) / ((M_TYPE) (i + 1)) ;
	}
}

void init_other(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = (M_TYPE) (i % 1000) ;
	}
}
void init_triv(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = (M_TYPE) i + i / 2;
	}
}
void init_incr(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = ((M_TYPE) i)  / 256. ;
	}
}

typedef enum res_check {SAME, REF_ZERO, VAL_ZERO, DIFFERENT} res_check_t;

res_check_t check_result(M_TYPE ref, M_TYPE found, M_TYPE max_known, M_TYPE epsilon) {
  M_TYPE delta = ABS(ref - found);
  M_TYPE max_val = MAX(ABS(ref),ABS(found));
  if (ref == 0. && found != 0.) {
    if (ABS(found) > epsilon * max_known) return  REF_ZERO;
    else return SAME;
  }
  if (ref != 0. && found == 0.) {
    if (ABS(ref) > epsilon * max_known) return  VAL_ZERO;
    else return SAME;
  }
  if (delta > epsilon * max_val) {
    return DIFFERENT;
  }
  return SAME;
}

cerror_info_t handle_res(cerror_info_t prev, IND_TYPE i, IND_TYPE j, IND_TYPE f, M_TYPE ref, M_TYPE computed) {
  cerror_info_t new_info = prev;
  new_info.num_test++;
  new_info.max_val = MAX(MAX(ABS(ref), ABS(computed)), new_info.max_val);
  M_TYPE delta = ABS(ref - computed);
  switch (check_result(ref, computed, prev.max_val, prev.epsilon)) {
    case SAME:
      break;
    case DIFFERENT:
      new_info.error_count = prev.error_count + 1;
      new_info.nonzero_count = prev.nonzero_count + 1;
      new_info.kind = NON_ZERO;
      switch (prev.kind) {
        case NO_ERROR:
        case ZERO_ERROR:
          new_info.error.nonzero.computed = computed;
          new_info.error.nonzero.ref = ref;
          new_info.error.nonzero.delta = delta;
          new_info.error.nonzero.i = i;
          new_info.error.nonzero.j = j;
          new_info.error.nonzero.f = f;
          break;
        case NON_ZERO:
          if (delta > prev.error.nonzero.delta) {
            new_info.error.nonzero.computed = computed;
            new_info.error.nonzero.ref = ref;
            new_info.error.nonzero.delta = delta;
            new_info.error.nonzero.i = i;
            new_info.error.nonzero.j = j;
			new_info.error.nonzero.f = f;
          }
          break;
      }
      break;
    case VAL_ZERO:
      new_info.error_count++;
      new_info.zero_count++;
      switch (prev.kind) {
        case NO_ERROR:
          new_info.kind = ZERO_ERROR;
          new_info.error.zero.is_ref_zero = false;
          new_info.error.zero.non_zero_value = ref;
          new_info.error.zero.i = i;
          new_info.error.zero.j = j;
          new_info.error.zero.f = f;
          break;
        case ZERO_ERROR:
          if (ABS(ref) > prev.error.zero.non_zero_value){
            new_info.kind = ZERO_ERROR;
            new_info.error.zero.is_ref_zero = false;
            new_info.error.zero.non_zero_value = ABS(ref);
            new_info.error.zero.i = i;
            new_info.error.zero.j = j;
            new_info.error.zero.f = f;
          }
          break;
        case NON_ZERO:
          break;
      }
      break;
    case REF_ZERO:
      new_info.error_count++;
      new_info.zero_count++;
      switch (prev.kind) {
        case NO_ERROR:
          new_info.kind = ZERO_ERROR;
          new_info.error.zero.is_ref_zero = true;
          new_info.error.zero.non_zero_value = ABS(computed);
          new_info.error.zero.i = i;
          new_info.error.zero.j = j;
          new_info.error.zero.f = f;
          break;
        case ZERO_ERROR:
          if (ABS(computed) > prev.error.zero.non_zero_value){
            new_info.kind = ZERO_ERROR;
            new_info.error.zero.is_ref_zero = true;
            new_info.error.zero.non_zero_value = ABS(computed);
            new_info.error.zero.i = i;
            new_info.error.zero.j = j;
            new_info.error.zero.f = f;
          }
          break;
        case NON_ZERO:
          break;
      }
      break;
  }
  return new_info;
}


cerror_info_t init_error_info(M_TYPE epsilon) {
  cerror_info_t info;
  info.max_val = 0.0000001;
  info.epsilon = epsilon;
  info.num_test = 0;
  info.error_count = 0;
  info.zero_count = 0;
  info.nonzero_count = 0;
  info.kind = NO_ERROR;
  info.error.null_state = 0;
  return info;
}

void print_eps_error(cerror_info_t err) {
  czero_error_t zerr ;
  cnonzero_error_t nzerr ;
  switch (err.kind) {
  case NO_ERROR:
    fprintf(stderr, "    No error found\n");
    break;
  case ZERO_ERROR:
    zerr = err.error.zero;
    fprintf(stderr, "    Found %d errors in %d tries\n", err.error_count, err.num_test);
    fprintf(stderr, "    Only zero were wrong\n");
    fprintf(stderr, "    max error found at: i: %ld, j: %ld\n", zerr.i, zerr.j);
    fprintf(stderr, "    Wrong val : %f scaled to max: %f\n", zerr.non_zero_value, zerr.non_zero_value / err.max_val);
    break;
  case NON_ZERO:
    fprintf(stderr, "    Found %d errors in %d tries made of\n", err.error_count, err.num_test);
    fprintf(stderr, "      %d zero errors (ref or computed value is zero)\n", err.zero_count);
    nzerr = err.error.nonzero;
    fprintf(stderr, "      %d standard errors (ref and computed value are not zero)\n", err.nonzero_count);
    fprintf(stderr, "    Max error found at i = %ld, j = %ld\n", nzerr.i, nzerr.j);
    fprintf(stderr, "      Max val = %f\n", nzerr.computed);
    fprintf(stderr, "      Max ref = %f\n", nzerr.ref);
    fprintf(stderr, "      Delta = %f\n", nzerr.delta);
    fprintf(stderr, "      Ratio = %f\n", nzerr.delta / MAX(ABS(nzerr.computed),ABS(nzerr.ref)));
    break;
  }
} 

int is_err(cerror_info_t err) {
  return (err.error_count > 0);
}


// Input accesses - different layouts
M_TYPE access_input(layout_in_t layout,
        M_TYPE * I,
        IND_TYPE NY, IND_TYPE NH, IND_TYPE NX, IND_TYPE NW, IND_TYPE NC,
        IND_TYPE y, IND_TYPE h, IND_TYPE x, IND_TYPE w, IND_TYPE c,
        IND_TYPE str_x, IND_TYPE str_y) {
    switch(layout) {
        case YXC:
            return I[(str_x * NX + NW - 1) * NC * (str_y * y + h) + NC * (str_x * x + w) + c]; 
        case XCY:
            return I[(str_y * NY + NH - 1) * NC * (str_x * x + w) + (str_y * NY + NH - 1) * c + (str_y * y + h) ]; 
        case XYC:
            return I[(str_y * NY + NH - 1) * NC * (str_x * x + w) + NC * (str_y * y + h) + c]; 
        case CYX:
            return I[(str_y * NY + NH - 1) * (str_x * NX + NW - 1) * c
                + (NX + NW - 1) * (str_y * y + h) + (str_x * x + w) ]; 
        case CYXc:
            perror("CYXc Unsupported");
            return -1;
        default:
            perror("Forgot one layout ?");
            return -1;
    }
}

// Params accesses

M_TYPE access_par(layout_par_t layout,
        M_TYPE * P,  IND_TYPE NH, IND_TYPE NW, IND_TYPE NC, IND_TYPE NF,
        IND_TYPE h, IND_TYPE w, IND_TYPE c,  IND_TYPE f) {
    switch(layout) {
        case HWCF:
            return P[NW * NC * NF * h  + NC * NF * w + NF * c + f]; 
        case WHCF:
            return P[NH * NC * NF * w  + NC * NF * h + NF * c + f]; 
        case FCHW:
            return P[NC * NH * NW * f  + NH * NW * c + NW * h + w]; 
        default:
            perror("Forgot one layout ?");
    }
}

M_TYPE access_output(layout_out_t layout,
        M_TYPE * O, IND_TYPE NF, IND_TYPE NY, IND_TYPE NX,
		IND_TYPE f, IND_TYPE y, IND_TYPE x) {
    switch(layout){
        case FYX:
            return O[NY * NX * f + NX * y + x]; 
        case YXF:
            return O[NX * NF * y + NF * x + f]; 
        case XYF:
            return O[NY * NF * x + NF * y + f]; 
        default:
            perror("Forgot one layout ?");
    }
}

#undef DEBUG_VERIF
cerror_info_t is_conv_sample(M_TYPE * I, M_TYPE * P, M_TYPE * O,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y,
        layout_in_t layout_in,
        layout_par_t layout_par,
        layout_out_t layout_out,
		int nb_check, cindice_t * to_check, M_TYPE epsilon) {
	int randx, randy, randf;
	int num_test = 0;
	cerror_info_t max_error = init_error_info(epsilon);
	while (num_test < nb_check) {
    randx = to_check[num_test].x;
    randy = to_check[num_test].y;
    randf = to_check[num_test].f;
    M_TYPE red = 0;
#ifdef DEBUG_VERIF
	fprintf(stderr, "C = %ld, w = %ld h = %ld\n", NC, NW, NH);
	fprintf(stderr, "coord : randx = %d, randy = %d randf = %d\n", randx, randy, randf);
#endif
    for (int c = 0; (uint64_t)c < NC; c++) {
        for (int w = 0; (uint64_t)w < NW; w++) {
            for (int h = 0; (uint64_t)h < NH; h++) {
#ifdef DEBUG_VERIF
				fprintf(stderr,
						"h = %d,w = %d,c = %d \n", h, w, c);
				fprintf(stderr,
						"red :  %f, in[h,randx,w,c] = %f,  p[w, h, c, randf] = %f\n",
						red, access_input(layout_in,
                            I, NY, NH, NX, NW, NC,
                            randy, h, randx, w, c, str_x, str_y),
						access_par(layout_par, P, NH, NW, NC, NF,  h , w,  c, randf));
#endif
                red +=
                    access_input(layout_in,
                            I, NY, NH, NX, NW, NC, randy, h, randx, w, c, str_x, str_y)
                    * access_par(layout_par, P, NH, NW, NC, NF,  h , w,  c, randf);
			}
		}
	}
#ifdef DEBUG_VERIF
    //fprintf(stderr, "red = %f\n", red);
#endif
    max_error = handle_res(max_error, randx, randy, randf, red,
            access_output(layout_out, O, NF, NY, NX, randf, randy, randx)
            );
    num_test++;
  }
	return max_error;
}
cexact_einfo_t init_exact_einfo() {
  cexact_einfo_t info;
  info.num_test = 0;
  info.err_count = 0;
  info.i = -1;
  info.j = -1;
  info.delta = -1.;
  info.val = -1.;
  info.ref = -1.;
  return info;
}

cexact_einfo_t handle_exact(cexact_einfo_t prev, IND_TYPE i, IND_TYPE j, IND_TYPE f,
		M_TYPE ref, M_TYPE computed) {
	cexact_einfo_t new_info = prev;
	new_info.num_test++;
	if (ref != computed ) {
		new_info.err_count++;
		if (ABS(ref - computed) > new_info.delta) {
			new_info.i = i;
			new_info.j = j;
			new_info.j = f;
			new_info.delta = ABS(ref - computed);
			new_info.val = computed;
			new_info.ref = ref;
		}
	}
	return new_info;
}

int is_err_exact(cexact_einfo_t info){
  return (info.err_count > 0);
}

void print_exact_error(cexact_einfo_t info) {
  if (info.err_count == 0) {
    fprintf(stderr, "    No error found\n");
    return;
  } else {
    fprintf(stderr, "    Found %d float errors in %d tries\n", info.err_count, info.num_test);
    fprintf(stderr, "    Max error found at i = %ld, j = %ld\n", info.i, info.j);
    fprintf(stderr, "      Max val = %f\n", info.val);
    fprintf(stderr, "      Max ref = %f\n", info.ref);
    fprintf(stderr, "      Delta = %f\n", info.delta);
  }
}

void print_rand_elem(M_TYPE * mat, IND_TYPE isize, IND_TYPE jsize, int nb_test) {
  IND_TYPE randi, randj;
  int num_test = 0;
  while (num_test < nb_test) {
    randi = rand() % isize;
    randj = rand() % jsize;
    printf("M[%ld, %ld] = %f\n", randi, randj, mat[jsize * randi + randj]);
    num_test++;
  }
}

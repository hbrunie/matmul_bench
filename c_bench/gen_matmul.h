#ifndef GEN_H
#include "matrix_utils.h"
#include "assert.h"
#include "mem_utils.h"
#include "gen/params.h"
#include <x86intrin.h>

void gen_matmul( M_TYPE const * const __restrict__ A, M_TYPE const * const __restrict__ B,
		M_TYPE * const  __restrict__ C,
    IND_TYPE I, IND_TYPE J, IND_TYPE K);
#endif

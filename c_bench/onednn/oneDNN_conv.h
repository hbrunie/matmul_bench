#ifndef ONEDNN_CONV_H
#define ONEDNN_CONV_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "../timing.h"

#include "dnn_utils.h"			// From the OneDNN library

// === Matrix sizes (where B=1) ===
// K      :: W * H * C * F
// Input  :: B * (X+W-1) * (Y+H-1) * C
// Output :: B * X * Y * F
//
// === Computation ===
// 		Output[ f, y, x] = K[f, c, h, w] * Input[ c, y+h, x+w]
//
// WARNING: different from:
//		Output[ x,y,f] = K[w,k,c,f] * Input[ x+w, y+h, c]


#ifdef NO_RECORD
// Implementation of the convolution, using the oneDNN framework
void conv_onednn_no_record(
        float * const __restrict__ K,
        float * const __restrict__ Input,
        float * const __restrict__ Output,
        const int W, const int H, const int C, const int F, const int X, const int Y);

#else
// Implementation of the convolution, using the oneDNN framework
void conv_onednn(
        papi_info_t info,
        long long* results,
        float * const __restrict__ K,
        float * const __restrict__ Input,
        float * const __restrict__ Output,
        const int W, const int H, const int C, const int F, const int X, const int Y,
        const int str_x, const int str_y );
#endif


// Naive implementation of the convolution (to check correctness)
void conv_naive_impl(float const * const __restrict__ K,
	float const * const __restrict__ Input,
	float * const __restrict__ Output,
	const int W, const int H, const int C, const int F, const int X, const int Y);




#endif

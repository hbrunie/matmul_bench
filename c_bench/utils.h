#ifndef UTILS_H
#define UTILS_H
#include <stdio.h>
#include <x86intrin.h>
#include <string.h>   

#define MAX(a,b) ((a) >= (b)?(a):(b))
#define MIN(a,b) ((a) <= (b)?(a):(b))

#define ABS(x) ((x)>=0?(x):-(x))

#endif

#include "matrix_utils.h"

void init_zero(M_TYPE * tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = 0.;
	}
}

void init_identity(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			if (i == j) {tab[i * rsize + j] = 1.;}
			else {tab[i * rsize + j] = 0.;}
		}
	}
}

void init_diag(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			if ((j >= i)|| (j < i + 10)) {tab[i * rsize + j] = 1.;}
			else {tab[i * rsize + j] = 0.;}
		}
	}
}

void init_bounded(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = (i *13 + j * 12) % bound;
			tab[i * rsize + j] = ((M_TYPE) val);
		}
	}
}

void init_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = rand() % bound;
			tab[i * rsize + j] = ((M_TYPE) val);
		}
	}
}

void init_signed_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			int64_t val = rand() % bound;
			tab[i * rsize + j] = (rand() & 1)? ((M_TYPE) val): ((M_TYPE) -val);
		}
	}
}

void init_rand_zero_one(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize) {
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
                  tab[i * rsize + j] = (M_TYPE)rand() / (M_TYPE)RAND_MAX;
		}
	}
}

void init_par(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = ((M_TYPE) i) / ((M_TYPE) (i + 1)) ;
	}
}

void init_other(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = (M_TYPE) (i % 1000) ;
	}
}
void init_triv(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = (M_TYPE) i + i / 2;
	}
}
void init_incr(M_TYPE* tab, IND_TYPE size) {
	for (IND_TYPE i = 0; i < size; i++) {
		tab[i] = ((M_TYPE) i)  / 256. ;
	}
}

// FINDING MAX
max_elem_t max_elem(M_TYPE * matrix, IND_TYPE csize, IND_TYPE rsize) {
	max_elem_t ret;
	M_TYPE max = -INFINITY;
	IND_TYPE maxi = 0;
	IND_TYPE maxj = 0;
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			if (matrix[i * rsize + j] > max) {
				max = matrix[i * rsize + j];
				maxi = i;
				maxj = j;
			}
		}
	}
	ret.i = maxi;
	ret.j = maxj;
	ret.val = max;
	return ret;
}


void basic_matmul( M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
		IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
	for (IND_TYPE i = 0; i < nI; i++){
		for (IND_TYPE j = 0; j < nJ; j++){
			for (IND_TYPE k = 0; k < nK; k++){
				c[i * nJ + j] = a[i * nK + k] * b[k * nJ + j];
			}
		}
	}
}
typedef enum res_check {SAME, REF_ZERO, VAL_ZERO, DIFFERENT} res_check_t;

res_check_t check_result(M_TYPE ref, M_TYPE found, M_TYPE max_known, M_TYPE epsilon) {
  M_TYPE delta = ABS(ref - found);
  M_TYPE max_val = MAX(ABS(ref),ABS(found));
  if (ref == 0. && found != 0.) {
    if (ABS(found) > epsilon * max_known) return  REF_ZERO;
    else return SAME;
  }
  if (ref != 0. && found == 0.) {
    if (ABS(ref) > epsilon * max_known) return  VAL_ZERO;
    else return SAME;
  }
  if (delta > epsilon * max_val) {
    return DIFFERENT;
  }
  return SAME;
}

error_info_t handle_res(error_info_t prev, IND_TYPE i, IND_TYPE j, M_TYPE ref, M_TYPE computed) {
  error_info_t new_info = prev;
  new_info.num_test++;
  new_info.max_val = MAX(MAX(ABS(ref), ABS(computed)), new_info.max_val);
  M_TYPE delta = ABS(ref - computed);
  switch (check_result(ref, computed, prev.max_val, prev.epsilon)) {
    case SAME:
      break;
    case DIFFERENT:
      new_info.error_count = prev.error_count + 1;
      new_info.nonzero_count = prev.nonzero_count + 1;
      new_info.kind = NON_ZERO;
      switch (prev.kind) {
        case NO_ERROR:
        case ZERO_ERROR:
          new_info.error.nonzero.computed = computed;
          new_info.error.nonzero.ref = ref;
          new_info.error.nonzero.delta = delta;
          new_info.error.nonzero.i = i;
          new_info.error.nonzero.j = j;
          break;
        case NON_ZERO:
          if (delta > prev.error.nonzero.delta) {
            new_info.error.nonzero.computed = computed;
            new_info.error.nonzero.ref = ref;
            new_info.error.nonzero.delta = delta;
            new_info.error.nonzero.i = i;
            new_info.error.nonzero.j = j;
          }
          break;
      }
      break;
    case VAL_ZERO:
      new_info.error_count++;
      new_info.zero_count++;
      switch (prev.kind) {
        case NO_ERROR:
          new_info.kind = ZERO_ERROR;
          new_info.error.zero.is_ref_zero = false;
          new_info.error.zero.non_zero_value = ref;
          new_info.error.zero.i = i;
          new_info.error.zero.j = j;
          break;
        case ZERO_ERROR:
          if (ABS(ref) > prev.error.zero.non_zero_value){
            new_info.kind = ZERO_ERROR;
            new_info.error.zero.is_ref_zero = false;
            new_info.error.zero.non_zero_value = ABS(ref);
            new_info.error.zero.i = i;
            new_info.error.zero.j = j;
          }
          break;
        case NON_ZERO:
          break;
      }
      break;
    case REF_ZERO:
      new_info.error_count++;
      new_info.zero_count++;
      switch (prev.kind) {
        case NO_ERROR:
          new_info.kind = ZERO_ERROR;
          new_info.error.zero.is_ref_zero = true;
          new_info.error.zero.non_zero_value = ABS(computed);
          new_info.error.zero.i = i;
          new_info.error.zero.j = j;
          break;
        case ZERO_ERROR:
          if (ABS(computed) > prev.error.zero.non_zero_value){
            new_info.kind = ZERO_ERROR;
            new_info.error.zero.is_ref_zero = true;
            new_info.error.zero.non_zero_value = ABS(computed);
            new_info.error.zero.i = i;
            new_info.error.zero.j = j;
          }
          break;
        case NON_ZERO:
          break;
      }
      break;
  }
  return new_info;
}


error_info_t init_error_info(M_TYPE epsilon) {
  error_info_t info;
  info.max_val = 0.0000001;
  info.epsilon = epsilon;
  info.num_test = 0;
  info.error_count = 0;
  info.zero_count = 0;
  info.nonzero_count = 0;
  info.kind = NO_ERROR;
  info.error.null_state = 0;
  return info;
}

void print_eps_error(error_info_t err) {
  zero_error_t zerr ;
  nonzero_error_t nzerr ;
  switch (err.kind) {
  case NO_ERROR:
    fprintf(stderr, "    No error found\n");
    break;
  case ZERO_ERROR:
    zerr = err.error.zero;
    fprintf(stderr, "    Found %d errors in %d tries\n", err.error_count, err.num_test);
    fprintf(stderr, "    Only zero were wrong\n");
    fprintf(stderr, "    max error found at: i: %ld, j: %ld\n", zerr.i, zerr.j);
    fprintf(stderr, "    Wrong val : %f scaled to max: %f\n", zerr.non_zero_value, zerr.non_zero_value / err.max_val);
    break;
  case NON_ZERO:
    fprintf(stderr, "    Found %d errors in %d tries made of\n", err.error_count, err.num_test);
    fprintf(stderr, "      %d zero errors (ref or computed value is zero)\n", err.zero_count);
    nzerr = err.error.nonzero;
    fprintf(stderr, "      %d standard errors (ref and computed value are not zero)\n", err.nonzero_count);
    fprintf(stderr, "    Max error found at i = %ld, j = %ld\n", nzerr.i, nzerr.j);
    fprintf(stderr, "      Max val = %f\n", nzerr.computed);
    fprintf(stderr, "      Max ref = %f\n", nzerr.ref);
    fprintf(stderr, "      Delta = %f\n", nzerr.delta);
    fprintf(stderr, "      Ratio = %f\n", nzerr.delta / MAX(ABS(nzerr.computed),ABS(nzerr.ref)));
    break;
  }
} 

int is_err(error_info_t err) {
  return (err.error_count > 0);
}

error_info_t is_matmul_sample(M_TYPE * A, M_TYPE * B, M_TYPE * C,
    IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK,
                              int nb_check, indice_t * to_check, M_TYPE epsilon) {
  int randi, randj;
  int num_test = 0;
  error_info_t max_error = init_error_info(epsilon);
  while (num_test < nb_check) {
    randi = to_check[num_test].i;
    randj = to_check[num_test].j;
    M_TYPE red = 0;
    for (int k = 0; k < NK; k++) {
      red += A[NK * randi + k] * B[NJ * k + randj];
    }
    max_error = handle_res(max_error, randi, randj, red, C[randi * NJ + randj]);
    num_test++;
  }
	return max_error;
}
exact_einfo_t init_exact_einfo() {
  exact_einfo_t info;
  info.num_test = 0;
  info.err_count = 0;
  info.i = -1;
  info.j = -1;
  info.delta = -1.;
  info.val = -1.;
  info.ref = -1.;
  return info;
}

exact_einfo_t handle_exact(exact_einfo_t prev, IND_TYPE i, IND_TYPE j,
     M_TYPE ref, M_TYPE computed) {
  exact_einfo_t new_info = prev;
  new_info.num_test++;
  if (ref != computed ) {
    new_info.err_count++;
    if (ABS(ref - computed) > new_info.delta) {
      new_info.i = i;
      new_info.j = j;
      new_info.delta = ABS(ref - computed);
      new_info.val = computed;
      new_info.ref = ref;
    }
  }
  return new_info;
}

int is_err_exact(exact_einfo_t info){
  return (info.err_count > 0);
}

exact_einfo_t is_matmul_sample_exact(M_TYPE * A, M_TYPE * B, M_TYPE * C,
                                     IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK,
                                     int nb_check, indice_t * to_check) {
  int randi, randj;
  int num_test = 0;
  exact_einfo_t max_error = init_exact_einfo();
  while (num_test < nb_check) {
    randi = to_check[num_test].i;
    randj = to_check[num_test].j;
    float red = 0.;
    for (int k = 0; k < NK; k++) {
      red += A[NK * randi + k] * B[NJ * k + randj];
    }
    max_error = handle_exact(max_error, randi, randj, red, C[NJ * randi + randj]);
    num_test++;
  }
	return max_error;
}

void print_exact_error(exact_einfo_t info) {
  if (info.err_count == 0) {
    fprintf(stderr, "    No error found\n");
    return;
  } else {
    fprintf(stderr, "    Found %d float errors in %d tries\n", info.err_count, info.num_test);
    fprintf(stderr, "    Max error found at i = %ld, j = %ld\n", info.i, info.j);
    fprintf(stderr, "      Max val = %f\n", info.val);
    fprintf(stderr, "      Max ref = %f\n", info.ref);
    fprintf(stderr, "      Delta = %f\n", info.delta);
  }
}

#undef PRINT_WRONG_LINE_COLUMN
// Check function
int is_matmul(M_TYPE * A, M_TYPE * B, M_TYPE * C, IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK) {
	M_TYPE * Check = (M_TYPE *)aligned_alloc(32, sizeof(M_TYPE) * NI * NJ);
	init_zero(Check, NI * NJ);
	for (IND_TYPE i = 0; i < NI; i++) {
		for (IND_TYPE j = 0; j < NJ; j++) {
			for (IND_TYPE k = 0; k < NK; k++) {
				Check[i * NJ + j] += A[i * NK + k] * B[k * NJ + j];
			}
		}
	}
	int err_count = 0;
	M_TYPE max_error = 0.;
	for (IND_TYPE i = 0; i < NI; i++) {
		for (IND_TYPE j = 0; j < NJ; j++) {
			M_TYPE delta = ABS(Check[i * NJ + j] - C[i * NJ + j]);
			M_TYPE max_c = MAX(ABS(C[i * NJ + j]), ABS(Check[i* NJ + j]));
			if (delta > EPSILON * max_c) {
				max_error = (delta > max_error)? delta : max_error;
#ifdef PRINT_WRONG_LINE_COLUMN
				if (err_count == 0) {
					printf("i, j : %ld, %ld\n", i, j);
					printf("A[i, ..]: [");
					for (IND_TYPE k = 0; k < NK; k++) {
						printf("%d, ", (int)A[i * NK + k]);
					}
					printf("]\n ");
					printf("B[.., j]: [");
					for (IND_TYPE k = 0; k < NK; k++) {
						printf("%d, ", (int)B[k * NJ + j]);
					}
					printf("]\n");
					printf("C[i, j] = %f\n", C[i * NJ + j]);
					printf("\n");
					printf("Check[i, j] = %f\n", Check[i * NJ + j]);
				}
#endif
				err_count++;
			}
		}
	}
	max_elem_t max_check = max_elem(Check, NI, NJ);
	max_elem_t max_c = max_elem(C, NI, NJ);
	printf("Matrix size: %ld, Error count : %d max error : %f \n", NI * NJ, err_count, max_error);
	if (err_count > 0) {
		printf("Max C: val: %f at %ld %ld,\nMax Check : %f at %ld %ld\nmax error / max_val : %f \n",
				max_c.val, max_c.i, max_c.j,
				max_check.val, max_check.i,max_check.j,
				max_error / max_c.val);
	}
  free(Check);
	return (err_count == 0);
}

void print_rand_elem(M_TYPE * mat, IND_TYPE isize, IND_TYPE jsize, int nb_test){
  IND_TYPE randi, randj;
  int num_test = 0;
  while (num_test < nb_test) {
    randi = rand() % isize;
    randj = rand() % jsize;
    printf("M[%ld, %ld] = %f\n", randi, randj, mat[jsize * randi + randj]);
    num_test++;
  }
}

// makes the compiler thinks we actually read tab, even if we don't
void fake_read(FILE* file, M_TYPE* tab, IND_TYPE size) {
	if (tab[0] + tab[0] < -1) {
		for (IND_TYPE i = 0; i < size; i++) {
			fprintf(file, "%f\n", tab[i] );
		}
	}
}

void fprint_mat(FILE* file, M_TYPE* tab, IND_TYPE csize, IND_TYPE rsize) {
	printf("rsize %ld, csize %ld\n", rsize, csize);
	for (IND_TYPE i = 0; i < csize; i++) {
		for (IND_TYPE j = 0; j < rsize; j++) {
			fprintf(file, "%f ", tab[i * rsize + j] );
		}
		fprintf(file, "\n");
	}
  fflush(file);
}


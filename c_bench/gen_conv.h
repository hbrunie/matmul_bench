#ifndef GEN_CONV_H
#define GEN_CONV_H
#include "conv_utils.h"
#include "assert.h"
#include "mem_utils.h"
#include "gen/params.h"
//#include "reorder_avx2.h"
#include <x86intrin.h>

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F);
#endif

// Common parts between matrix_utils and conv_utils
#ifndef COMMON_H
#define COMMON_H
#include <stdbool.h>
#include <stdint.h>

#undef USE_DOUBLE
#ifdef USE_DOUBLE
#define M_TYPE double
#else
#define M_TYPE float
#endif
#define IND_TYPE uint64_t

typedef enum error_kind {NO_ERROR, ZERO_ERROR, NON_ZERO} error_kind_t;

#endif

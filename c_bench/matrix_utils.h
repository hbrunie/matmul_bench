#ifndef MAT_UTILS_H
#define MAT_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>

#include "utils.h"
#include "gen/params.h"

#undef USE_DOUBLE
#ifdef USE_DOUBLE
#define M_TYPE double
#else
#define M_TYPE float
#endif
#define IND_TYPE uint64_t

//#define EPSILON 0.000001

// Initialization
void init_zero(M_TYPE * tab, IND_TYPE size);
void init_identity(M_TYPE* tab, IND_TYPE csize, IND_TYPE rsize);
void init_diag(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize);
void init_incr(M_TYPE * tab, IND_TYPE size) ;
void init_triv(M_TYPE * tab, IND_TYPE size) ;
void init_other(M_TYPE * tab, IND_TYPE size) ;
void init_par(M_TYPE * tab, IND_TYPE size) ;
void init_bounded(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_signed_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_rand_zero_one(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize);

// Find max elem
typedef struct indice {IND_TYPE i; IND_TYPE j;} indice_t;
typedef struct max_elem {IND_TYPE i; IND_TYPE j; M_TYPE val;} max_elem_t;
max_elem_t max_elem(M_TYPE * matrix, IND_TYPE csize, IND_TYPE rsize);

typedef enum error_kind {NO_ERROR, ZERO_ERROR, NON_ZERO} error_kind_t;
typedef struct nonzero_error {IND_TYPE i; IND_TYPE j; M_TYPE delta; M_TYPE ref; M_TYPE computed;} nonzero_error_t;
typedef struct zero_error {IND_TYPE i; IND_TYPE j; bool is_ref_zero; M_TYPE non_zero_value;} zero_error_t;
typedef union error_type {char null_state; nonzero_error_t nonzero; zero_error_t zero;} error_type_t;
typedef struct error_info {M_TYPE epsilon; M_TYPE max_val;  int num_test; int error_count; int zero_count;
  int nonzero_count; error_kind_t kind; error_type_t error;} error_info_t;

typedef struct exact_einfo {int num_test; int err_count;
  IND_TYPE i; IND_TYPE j;
  M_TYPE val; M_TYPE ref; M_TYPE delta; 
  } exact_einfo_t;

exact_einfo_t is_matmul_sample_exact(M_TYPE * A, M_TYPE * B, M_TYPE * C,
    IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK, int nb_check, indice_t * to_check);
void print_exact_error(exact_einfo_t info);
int is_err_exact(exact_einfo_t info);

// MAT MULT operation
void basic_matmul( M_TYPE* __restrict__ a, M_TYPE* __restrict__ b, M_TYPE* const __restrict__ c,
		IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK);

// check C = A * B ?
int is_matmul(M_TYPE * A, M_TYPE * B, M_TYPE * C, IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK);
error_info_t is_matmul_sample(M_TYPE * A, M_TYPE * B, M_TYPE * C, IND_TYPE NI, IND_TYPE NJ, IND_TYPE NK,
                              int num_check, indice_t * to_check, M_TYPE epsilon);
int is_err(error_info_t err);
void print_eps_error(error_info_t err);

void print_rand_elem(M_TYPE * mat, IND_TYPE isize, IND_TYPE jsize, int nb_test);
// miscellaneous IO 
void fake_read(FILE* file, M_TYPE* tab, IND_TYPE size);
void fprint_mat(FILE* file, M_TYPE* tab, IND_TYPE csize, IND_TYPE rsize);
#endif

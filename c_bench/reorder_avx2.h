#ifndef REORDER_AVX2_H
#define REORDER_AVX2_H

#include <x86intrin.h>

//__attribute__((always_inline)) void transpose_base_stride_avx2(float* __restrict__ base_in, int stride_in, float* __restrict__ base_out, int stride_out);

__attribute__((always_inline)) void transpose_base_stride_avx2(float* __restrict__ base_in, int stride_in,
        float* __restrict__ base_out, int stride_out) {
    __m256  t0, t1, t2, t3, t4, t5, t6, t7;
    __m256  r0, r1, r2, r3, r4, r5, r6, r7;

    r0 = _mm256_load_ps(base_in);
    r1 = _mm256_load_ps(base_in + stride_in);
    r2 = _mm256_load_ps(base_in + 2 * stride_in);
    r3 = _mm256_load_ps(base_in + 3 * stride_in);
    r4 = _mm256_load_ps(base_in + 4 * stride_in);
    r5 = _mm256_load_ps(base_in + 5 * stride_in);
    r6 = _mm256_load_ps(base_in + 6 * stride_in);
    r7 = _mm256_load_ps(base_in + 7 * stride_in);

    t0 = _mm256_unpacklo_ps(r0, r1);
    t1 = _mm256_unpackhi_ps(r0, r1);
    t2 = _mm256_unpacklo_ps(r2, r3);
    t3 = _mm256_unpackhi_ps(r2, r3);
    t4 = _mm256_unpacklo_ps(r4, r5);
    t5 = _mm256_unpackhi_ps(r4, r5);
    t6 = _mm256_unpacklo_ps(r6, r7);
    t7 = _mm256_unpackhi_ps(r6, r7);

    r0 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(1,0,1,0));  
    r1 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(3,2,3,2));
    r2 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(1,0,1,0));
    r3 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(3,2,3,2));
    r4 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(1,0,1,0));
    r5 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(3,2,3,2));
    r6 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(1,0,1,0));
    r7 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(3,2,3,2));

    t0 = _mm256_permute2f128_ps(r0, r4, 0x20);
    t1 = _mm256_permute2f128_ps(r1, r5, 0x20);
    t2 = _mm256_permute2f128_ps(r2, r6, 0x20);
    t3 = _mm256_permute2f128_ps(r3, r7, 0x20);
    t4 = _mm256_permute2f128_ps(r0, r4, 0x31);
    t5 = _mm256_permute2f128_ps(r1, r5, 0x31);
    t6 = _mm256_permute2f128_ps(r2, r6, 0x31);
    t7 = _mm256_permute2f128_ps(r3, r7, 0x31);

    _mm256_store_ps(base_out, t0);
    _mm256_store_ps(base_out + stride_out, t1);
    _mm256_store_ps(base_out + 2 * stride_out, t2);
    _mm256_store_ps(base_out + 3 * stride_out, t3);
    _mm256_store_ps(base_out + 4 * stride_out, t4);
    _mm256_store_ps(base_out + 5 * stride_out, t5);
    _mm256_store_ps(base_out + 6 * stride_out, t6);
    _mm256_store_ps(base_out + 7 * stride_out, t7);
}

#endif

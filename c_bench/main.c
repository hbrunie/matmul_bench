#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <x86intrin.h>
#include <assert.h>
#include <papi.h>
#include "timing.h"

#ifdef RUI
#include "rui_conv/ukr10x3vCnnb1f128x112y112c128r3s3.h"
#endif

#include "gen/params.h"
#include "handle_counters.h"
#include "mem_utils.h"
#include <time.h>

#ifdef BLIS
#include <blis/blis.h>
#endif

#ifdef USE_CONV
#include "gen_conv.h"
#else
#include "gen_matmul.h"
#endif

#ifdef MKL
#include <mkl.h>
#endif


typedef enum {CONV, MM} kernel_type;

typedef void (mm_kernel_t) (
        papi_info_t info,
        long long* results,
        M_TYPE * __restrict__ c,
        M_TYPE * __restrict__ a, M_TYPE* __restrict__ b,
        IND_TYPE x_size, IND_TYPE y_size,
        IND_TYPE k_size);

typedef void (conv_kernel_t) (
        papi_info_t info,
        long long* results,
        M_TYPE * __restrict__ output,
		M_TYPE * __restrict__ input, M_TYPE* __restrict__ params,
		IND_TYPE x_size, IND_TYPE w_size,
		IND_TYPE y_size, IND_TYPE h_size,
		IND_TYPE c_size, IND_TYPE f_size,
		IND_TYPE str_x, IND_TYPE str_y);

typedef struct mm_args {
	mm_kernel_t* kernel;
	M_TYPE * a;
	M_TYPE * b;
	M_TYPE * c;
	IND_TYPE nx; IND_TYPE ny; IND_TYPE nk;
} mm_args_t;

typedef struct conv_args {
    conv_kernel_t* kernel;
    M_TYPE * output;
    M_TYPE * input;
    M_TYPE * params;
    IND_TYPE nx; IND_TYPE nw;
    IND_TYPE ny; IND_TYPE nh;
    IND_TYPE nc; IND_TYPE nf;
    IND_TYPE str_x; IND_TYPE str_y;
} conv_args_t;

typedef struct kernel_args {kernel_type typ;
	union {mm_args_t mm_args; conv_args_t conv_args;} args;
} kernel_args_t;

kernel_args_t build_conv_args(
	M_TYPE * output,
	M_TYPE * input,
	M_TYPE * params,
	IND_TYPE nx, IND_TYPE nw,
	IND_TYPE ny, IND_TYPE nh,
	IND_TYPE nc, IND_TYPE nf,
	IND_TYPE str_x, IND_TYPE str_y,
	conv_kernel_t kernel) {
	kernel_args_t kargs;
	conv_args_t cargs;
	cargs.output = output;
	cargs.input = input;
	cargs.params = params;
	cargs.nx = nx;
	cargs.nw = nw;
	cargs.ny = ny;
	cargs.nh = nh;
	cargs.nc = nc;
	cargs.nf = nf;
	cargs.str_x = str_x;
	cargs.str_y = str_y;
	cargs.kernel = kernel;
	kargs.args.conv_args = cargs;
	kargs.typ = CONV;
	return kargs;
}

kernel_args_t build_mm_args(
	M_TYPE * c,
	M_TYPE * a,
	M_TYPE * b,
	IND_TYPE nx,
	IND_TYPE ny,
	IND_TYPE nk,
	mm_kernel_t kernel) {
	kernel_args_t kargs;
	mm_args_t margs;
	margs.c = c;
	margs.a = a;
	margs.b = b;
	margs.nx = nx;
	margs.ny = ny;
	margs.nk = nk;
	margs.kernel = kernel;
	kargs.args.mm_args = margs;
	kargs.typ = MM;
	return kargs;
}

// TODO replace this boilerplate by proper generic polymorphism
void init_kernel(kernel_args_t *k_args) {
	mm_args_t mm_args;
	conv_args_t conv_args;
	switch (k_args->typ ) {
		case MM :
			mm_args = (k_args->args).mm_args;
			init_zero(mm_args.c, mm_args.nx * mm_args.ny);
			break;
		case CONV :
			conv_args =  k_args->args.conv_args;
			init_zero(conv_args.output, conv_args.nx * conv_args.ny * conv_args.nf);
			break;
	}
}

void set_mm_kernel(kernel_args_t *k_args, mm_kernel_t kernel) {
	assert(k_args->typ == MM);
	k_args->args.mm_args.kernel = kernel;
}

void set_conv_kernel(kernel_args_t *k_args, conv_kernel_t kernel) {
	assert(k_args->typ == CONV);
	k_args->args.conv_args.kernel = kernel;
}

__attribute__((always_inline)) void exec_kernel_rep(kernel_args_t *k_args, int num_rep, papi_info_t info, long long * results) {
	mm_args_t mm_args;
	conv_args_t conv_args;
	switch (k_args->typ) {
		case MM:
			mm_args = k_args->args.mm_args;
            for (int r = 0; r < num_rep; r++) {
                init_kernel(k_args);
                mm_args.kernel(
                        info, (long long *)&results[r * info.num_events],
                        mm_args.a, mm_args.b, mm_args.c,
                        mm_args.nx, mm_args.ny, mm_args.nk);
            }
			break;
		case CONV:
			conv_args =  k_args->args.conv_args;
            for (int r = 0; r < num_rep; r++) {
                init_kernel(k_args);
                conv_args.kernel(
                        info, (long long *)&results[r * info.num_events],
                        conv_args.output, conv_args.input, conv_args.params,
                        conv_args.nx, conv_args.nw,
                        conv_args.ny, conv_args.nh,
                        conv_args.nc, conv_args.nf,
                        conv_args.str_x, conv_args.str_y
						);
            }
			break;
	}
}

#ifdef RUI
void rui_conv(papi_info_t info, long long * results,
        M_TYPE * I, M_TYPE * P, M_TYPE * O,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF) {
    record_events(info);
    ukr10x3vCnnb1f128x112y112c128r3s3(I, P, O, 0);
    retrieve_results(info, results);
}
#endif

#ifdef USE_CONV
#ifdef ONEDNN
#include "onednn/oneDNN_conv.h"
void onednn( papi_info_t info, long long * results,
        M_TYPE * O, M_TYPE * I, M_TYPE * P,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y) {
    conv_onednn(info, results, P, I, O, NW, NH, NC, NF, NX, NY,
			str_x, str_y);
}
#endif
#ifdef GEN
void gen_wrap_conv(papi_info_t info, long long * results,
        M_TYPE * O, M_TYPE * I, M_TYPE * P,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y) {
    record_events(info);
    gen_conv(O, I,P,
		 NX, NW,
		 NY, NH,
		 NC, NF);
    retrieve_results(info, results);
}
#endif

#else // USE_CONV
#ifdef GEN
void gen_wrap_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    record_events(info);
    gen_matmul(
            a, b,  c,
            nI, nJ, nK);
    retrieve_results(info, results);
}
#endif

#ifdef BLIS
// A[i, k], B[k, j] and C[i, j] are all stored row-major
void blis_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    // Set the scalars to use.
    M_TYPE alpha, beta;
    IND_TYPE rsc, csc, rsa, csa, rsb, csb;
    alpha = 1.0;
    beta  = 0.;
    // Row-major, so the stride to from a column to the next one is 1,
    // stride from a row to another is sizeof(row)
    rsa = nK; csa = 1;
    rsb = nJ; csb = 1;
    rsc = nJ; csc = 1;
    // c := beta * c + alpha * a * b, where 'a', 'b', and 'c' are general.
    // RECORD COUNTERS
    record_events(info);

#ifdef USE_DOUBLE
    bli_dgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
            nI, nJ, nK,
            &alpha,
            a, rsa, csa,
            b, rsb, csb,
            &beta,
            c, rsc, csc);
#else
    bli_sgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
            nI, nJ, nK,
            &alpha,
            a, rsa, csa,
            b, rsb, csb,
            &beta,
            c, rsc, csc);
#endif
    // WRITE COUNTER RESULTS
    retrieve_results(info, results);
}
#endif //BLIS

#ifdef MKL
void mkl_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    MKL_INT         n, k;
    MKL_INT         lda, ldb, ldc;
    CBLAS_LAYOUT    layout;
    float           alpha, beta;
    alpha = 1.0;
    beta  = 0.;
    CBLAS_TRANSPOSE transp = CblasNoTrans;
    layout = CblasRowMajor;
    lda = nK;
    ldb = nJ;
    ldc = nJ;
    // RECORD COUNTERS
    record_events(info);
#ifdef USE_DOUBLE
    cblas_sgemm(layout, transp, transp, nI, nJ, nK, alpha, a, lda,
            b, ldb, beta, c, ldc);
#else
    cblas_sgemm(layout, transp, transp, nI, nJ, nK, alpha, a, lda,
            b, ldb, beta, c, ldc);
#endif
    // WRITE COUNTER RESULTS
    retrieve_results(info, results);
}
#endif
#endif // NOT USE CONV

int compare_long(const void * i1, const void * i2) {
    long long l1, l2;
    l1 = *(long long *) i1;
    l2 = *(long long *) i2;
    return (l1 - l2);
}

long long median_long_inplace(long long * results, int n_run) {
    qsort(results, n_run, sizeof(long long), compare_long);
    long long res = results[n_run / 2];
    return res;
}



void many_try(int64_t rep,
		kernel_args_t * kargs,
        long long* counter_results
        ) {
    papi_info_t papi_info = build_papi_info();
    size_t num_events = papi_info.num_events;
    long long results [rep][num_events];
    exec_kernel_rep(kargs, rep, papi_info, (long long*)results);
    //for (int r = 0; r < rep; r++) {
    //    init_kernel(kargs);
    //    record_events(papi_info);
	//	exec_kernel(kargs);
    //    retrieve_results(papi_info, (long long *)results[r]);
    //}
    long long intermediate[rep];
    for (size_t i = 0; i < num_events; i++) {
        for (int r = 0; r < rep; r++) {
            intermediate[r] = results[r][i];
        }
#ifdef PRINT_RESULTS
        fprintf(stderr, "UNSORTED INTERMEDIATE ");
        for (int r = 0; r < rep  ; r++) {
            fprintf(stderr, "%lld ", intermediate[r]);
        }
        fprintf(stderr, "\n");
#endif
        qsort(intermediate, rep, sizeof(long long), compare_long);
        counter_results[i] = intermediate[rep / 2];
    }
}


void get_mm_sizes(int argc, char** argv, int* i, int* j, int* k) {
    assert(argc == 4);
    char* ptr;
    *i = strtol(argv[1], &ptr, 10);
    assert(*ptr == '\0');
    *j = strtol(argv[2], &ptr, 10);
    assert(*ptr == '\0');
    *k = strtol(argv[3], &ptr, 10);
    assert(*ptr == '\0');
}

void get_conv_sizes(int argc, char** argv, int* i, int* w, int* j, int* h, int* c, int* f,
		int* str_x, int* str_y) {
	assert(argc == 7 || argc == 9);
	char* ptr;
	*i = strtol(argv[1], &ptr, 10);
	assert(*ptr == '\0');
	*w = strtol(argv[2], &ptr, 10);
	assert(*ptr == '\0');
	*j = strtol(argv[3], &ptr, 10);
	assert(*ptr == '\0');
	*h = strtol(argv[4], &ptr, 10);
	assert(*ptr == '\0');
	*c = strtol(argv[5], &ptr, 10);
	assert(*ptr == '\0');
	*f = strtol(argv[6], &ptr, 10);
	assert(*ptr == '\0');
	if (argc == 9) {
		*str_x = strtol(argv[7], &ptr, 10);
		assert(*ptr == '\0');
		*str_y = strtol(argv[8], &ptr, 10);
		assert(*ptr == '\0');
	} else {
		*str_x = 1;
		*str_y = 1;
	}
}

int main(int argc, char** argv) {
#ifdef ERROR_EPS
	M_TYPE epsilon = EPSILON;
#ifdef USE_CONV
	cerror_info_t err;
#else
	error_info_t err;
#endif
#endif
#ifdef ERROR_EXACT
#ifdef USE_CONV
	cexact_einfo_t exact_err;
#else
	exact_einfo_t exact_err;
#endif
#endif



    init_papi();
#ifdef USE_CUSTOM_ALLOC
    mem_init();
#endif

    //M_TYPE* m =  mat;
	kernel_args_t kargs;
#ifdef USE_CONV
    int x_size, y_size, w_size, h_size, c_size, f_size, str_x, str_y;
    srand(time(NULL));

    get_conv_sizes(argc, argv, &x_size, &w_size, &y_size, &h_size, &c_size, &f_size, &str_x, &str_y);
	IND_TYPE out_size = x_size * y_size * f_size; 
	IND_TYPE in_size = (str_x * x_size + w_size - 1) * (str_y * y_size + h_size - 1) * c_size; 
	IND_TYPE params_size = w_size * h_size * f_size * c_size; 
    M_TYPE* m =  (M_TYPE *)ALLOC(64,
			(out_size + in_size + params_size) * sizeof(M_TYPE));

    M_TYPE * output = m;
    M_TYPE * input = output + out_size;
    M_TYPE * params = input + in_size;
	kargs = build_conv_args(output, input, params,
			x_size, w_size, y_size, h_size,
			c_size, f_size,
			str_x, str_y,
			NULL);
    if (m == NULL) {
        fprintf(stderr, "failed to allocate buffers\n");
        return -1;
    }
    fflush(stderr);
    // INIT matrices
    init_zero(output, out_size);
    init_rand_zero_one(input, in_size);
    init_rand_zero_one(params, params_size);

    size_t nb_check = 1000;
    cindice_t* to_check = malloc(sizeof(cindice_t) * nb_check);
    for(size_t idx = 0; idx < nb_check; idx++) {
        to_check[idx].x = rand() % x_size;
        to_check[idx].y = rand() % y_size;
        to_check[idx].f = rand() % f_size;
    }

#else // USE_CONV
    int i_size, j_size, k_size;
    get_mm_sizes(argc, argv, &i_size, &j_size, &k_size);
    M_TYPE* m =  (M_TYPE *)ALLOC(64,
			(i_size * j_size + i_size * k_size + j_size * k_size) * sizeof(M_TYPE));
    M_TYPE * c = m;
    M_TYPE * a = c + i_size * j_size;
    M_TYPE * b = a + i_size * k_size;
	kargs = build_mm_args(c, a, b, i_size, j_size, k_size, NULL);

    srand(time(NULL));
    size_t nb_check = 1000;
    init_zero(c, i_size * j_size);
    init_rand_zero_one(a, i_size, k_size);
    init_rand_zero_one(b, k_size, j_size);
    indice_t* to_check = malloc(sizeof(indice_t) * nb_check);
    for(size_t idx = 0; idx < nb_check; idx++) {
        to_check[idx].i = rand() % i_size;
        to_check[idx].j = rand() % j_size;
    }
#endif

    init_papi();


#ifdef USE_CONV

#ifdef GEN
    long long conv_ctr_results[N_CTR];
	set_conv_kernel(&kargs, gen_wrap_conv);
    many_try(NUM_REP, &kargs, conv_ctr_results);
    print_counters(conv_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_conv_sample_exact(a, b, c, x_size, y_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_conv_sample(input, params, output,
			x_size, w_size,
			y_size, h_size,
			c_size, f_size,
			str_x, str_y,
            GEN_LAYOUT_IN,
            GEN_LAYOUT_PAR,
            GEN_LAYOUT_OUT,
            nb_check,
			to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // GEN



#ifdef ONEDNN
    long long onednn_ctr_results[N_CTR];
	set_conv_kernel(&kargs, onednn);
    many_try(NUM_REP, &kargs, onednn_ctr_results);
    print_counters(onednn_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_conv_sample_exact(a, b, c, x_size, y_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_conv_sample(input, params, output, x_size, w_size, y_size, h_size, c_size, f_size,
			str_x, str_y,
            DNN_LAYOUT_IN,
            DNN_LAYOUT_PAR,
            DNN_LAYOUT_OUT,
            nb_check,
			to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS

#endif // ONEDNN

#else // USE_CONV

#ifdef BLIS
    long long blis_ctr_results[N_CTR];
#endif// BLIS
#ifdef MKL
    long long mkl_ctr_results[N_CTR];
#endif //MKL
#ifdef GEN
    long long gen_ctr_results[N_CTR];
#endif // GEN

#ifdef BLIS
	set_mm_kernel(&kargs, blis_matmul);
    many_try(NUM_REP, &kargs, blis_ctr_results);
    print_counters(blis_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // BLIS

#ifdef MKL
	set_mm_kernel(&kargs, mkl_matmul);
    many_try(NUM_REP, &kargs, mkl_ctr_results);
    print_counters(mkl_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // MKL

#ifdef GEN
	set_mm_kernel(&kargs, gen_wrap_matmul);
    many_try(NUM_REP, &kargs, gen_ctr_results);
    print_counters(gen_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif// ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // GEN
#endif // USE_CONV ???

    fflush(stdout);

    //FREE(m);

#ifdef USE_CUSTOM_ALLOC
    mem_close();
#endif
}

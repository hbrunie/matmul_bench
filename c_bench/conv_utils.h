#ifndef MAT_UTILS_H
#define MAT_UTILS_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "utils.h"
#include "common.h"

// Initialization
void init_zero(M_TYPE * tab, IND_TYPE size);
void init_identity(M_TYPE* tab, IND_TYPE csize, IND_TYPE rsize);
void init_diag(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize);
void init_incr(M_TYPE * tab, IND_TYPE size) ;
void init_triv(M_TYPE * tab, IND_TYPE size) ;
void init_other(M_TYPE * tab, IND_TYPE size) ;
void init_par(M_TYPE * tab, IND_TYPE size) ;
void init_bounded(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_signed_bounded_rand(M_TYPE * tab, IND_TYPE csize, IND_TYPE rsize, int64_t bound);
void init_rand_zero_one(M_TYPE * tab, IND_TYPE size);

// Find max elem
typedef struct cindice {IND_TYPE x; IND_TYPE y; IND_TYPE f;} cindice_t;

typedef struct cnonzero_error {IND_TYPE i; IND_TYPE j; IND_TYPE f; M_TYPE delta; M_TYPE ref; M_TYPE computed;} cnonzero_error_t;
typedef struct czero_error {IND_TYPE i; IND_TYPE j; IND_TYPE f; bool is_ref_zero; M_TYPE non_zero_value;} czero_error_t;
typedef union cerror_type {char null_state; cnonzero_error_t nonzero; czero_error_t zero;} cerror_type_t;
typedef struct cerror_info {M_TYPE epsilon; M_TYPE max_val;  int num_test; int error_count; int zero_count;
  int nonzero_count; error_kind_t kind; cerror_type_t error;} cerror_info_t;

typedef struct cexact_einfo {int num_test; int err_count;
  IND_TYPE i; IND_TYPE j; IND_TYPE f;
  M_TYPE val; M_TYPE ref; M_TYPE delta; 
  } cexact_einfo_t;


cexact_einfo_t is_conv_sample_exact(M_TYPE * I, M_TYPE * P, M_TYPE * O,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		 int nb_check, cindice_t * to_check);
void print_exact_error(cexact_einfo_t info);
int is_err_exact(cexact_einfo_t info);

typedef enum layout_in {YXC, XYC, CYX, XCY, CYXc} layout_in_t;

typedef enum layout_par {HWCF, WHCF, FCHW} layout_par_t;

typedef enum layout_out {YXF, XYF, FYX} layout_out_t;

cerror_info_t is_conv_sample(M_TYPE * I, M_TYPE * P, M_TYPE * O,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y,
        layout_in_t in_acc,
        layout_par_t p_acc,
        layout_out_t out_acc,
		int nb_check, cindice_t * to_check, M_TYPE epsilon);

void print_eps_error(cerror_info_t err);
#endif

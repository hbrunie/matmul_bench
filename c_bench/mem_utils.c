#include <stdlib.h>
#include <assert.h>
#include <sys/mman.h>
#include "mem_utils.h"


void* ptr;
size_t nb_allocated_bytes ;
state_t state = NON_INIT;

#define FLAGS (MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB)
#define PROTECTION (PROT_READ | PROT_WRITE)

void mem_init() {
    assert (state == NON_INIT);
    void* alloc_ptr = mmap(0, INIT_LENGTH, PROTECTION, FLAGS, 0, 0);
    assert(alloc_ptr != MAP_FAILED);
    ptr = alloc_ptr;
    nb_allocated_bytes = 0;
    state = INIT;
}
void mem_close() {
    assert(state == INIT);
    munmap(ptr, 0);
    state = CLOSED;
}

void* custom_alloc(size_t align, size_t len) {
    assert(state == INIT);
    size_t next_boundary;
    if (nb_allocated_bytes % align == 0)
        next_boundary = nb_allocated_bytes;
    else
        next_boundary = (nb_allocated_bytes + align) / align * align;
    assert (next_boundary + len <= INIT_LENGTH);
    nb_allocated_bytes = next_boundary + len;
    return (ptr + next_boundary);
}

void* _custom_alloc(size_t align, size_t len) {
    return aligned_alloc(align, len);
}

void custom_free(void* _ptr) {
}

void _custom_free(void* ptr) {
    free(ptr);
}

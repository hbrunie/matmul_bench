From close_l2.csv, selected all above 80% then generated tiles like that :

```ocaml
let build_l3_tile (i, j, k) = let open MM in
let tsize = dim_tile_size little_tile in
let i_tile, j_tile, k_tile = i / tsize i_dim, j / tsize j_dim, k / tsize k_dim in
[V j_dim; U (2, j_dim); U (6, i_dim); T (k, k_dim); Hoist_vars [k_dim];
T (i_tile, i_dim); T (j_tile, j_dim); T (k_tile, k_dim);
 R j_dim;  R i_dim; R k_dim ]
```

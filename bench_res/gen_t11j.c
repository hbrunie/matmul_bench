#include "gen_matmul.h"

void gen_matmul( M_TYPE const * const __restrict__ A, M_TYPE const * const __restrict__ B,
		M_TYPE * const  __restrict__ C,
    IND_TYPE I, IND_TYPE J, IND_TYPE K) {
/*
[V j; U (2, j); U (6, i); T (128, k); Hoist_vars [k]; T (11, j); R i; Pack A;
  Pack B; R k; R j]
*/
int i, iall, j, j0, jp_0, jg0, jl0, k, k0, kp_0, kg0, kl0;
float * B0 = (float *)aligned_alloc(64, sizeof(float) * 45056);
float * A0 = (float *)aligned_alloc(64, sizeof(float) * I * 128);
assert((6 <= I) && (I % 6 == 0));
assert((352 <= J) && (J % 352 == 0));
assert((128 <= K) && (K % 128 == 0));
float scal_0 ,scal_1 ,scal_2 ,scal_3 ,scal_4 ,scal_5;
__m512 mem_vec_0 ,mem_vec_1 ,mem_vec_10 ,mem_vec_11 ,mem_vec_2 ,mem_vec_3 ,mem_vec_4 ,mem_vec_5 ,mem_vec_6 ,mem_vec_7 ,mem_vec_8 ,mem_vec_9 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_3 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
for (j0 = 0;
	j0 < J;
	j0 += 352){
	for (k0 = 0;
		k0 < K;
		k0 += 128){
		 //Pack A into A0
		for (iall = 0;
			iall < I;
			iall += 1){
			for (kg0 = k0, kl0 = 0;
				kg0 < MIN(k0 + 128, K);
				kg0 += 16, kl0 += 16){
				vec_0 = _mm512_load_ps(&A[K * iall + kg0]);;
				_mm512_store_ps(&A0[128 * iall + kl0], vec_0);;
			}
		}
		 //Pack B into B0
		for (kg0 = k0, kl0 = 0;
			kg0 < MIN(k0 + 128, K);
			kg0 += 1, kl0 += 1){
			for (jg0 = j0, jl0 = 0;
				jg0 < j0 + 352;
				jg0 += 16, jl0 += 16){
				vec_0 = _mm512_load_ps(&B[J * kg0 + jg0]);;
				_mm512_store_ps(&B0[352 * kl0 + jl0], vec_0);;
			}
		}
		for (i = 0;
			i < I;
			i += 6){
			 //Tiling dim j by 11
			for (j = j0, jp_0 = 0;
				j < MIN(j0 + 352, J);
				j += 32, jp_0 += 32){
				mem_vec_11 = _mm512_load_ps(&C[J * (i + 5) + j + 16]);;
				mem_vec_9 = _mm512_load_ps(&C[J * (i + 4) + j + 16]);;
				mem_vec_7 = _mm512_load_ps(&C[J * (i + 3) + j + 16]);;
				mem_vec_5 = _mm512_load_ps(&C[J * (i + 2) + j + 16]);;
				mem_vec_3 = _mm512_load_ps(&C[J * (i + 1) + j + 16]);;
				mem_vec_1 = _mm512_load_ps(&C[J * i + j + 16]);;
				mem_vec_0 = _mm512_load_ps(&C[J * i + j]);;
				mem_vec_2 = _mm512_load_ps(&C[J * (i + 1) + j]);;
				mem_vec_4 = _mm512_load_ps(&C[J * (i + 2) + j]);;
				mem_vec_6 = _mm512_load_ps(&C[J * (i + 3) + j]);;
				mem_vec_8 = _mm512_load_ps(&C[J * (i + 4) + j]);;
				mem_vec_10 = _mm512_load_ps(&C[J * (i + 5) + j]);;
				 //Tiling dim k by 128
				for (k = k0, kp_0 = 0;
					k < k0 + 128;
					k += 1, kp_0 += 1){
					scal_0 = A0[kp_0];
					vec_1 = _mm512_set1_ps(scal_0);
					vec_2 = _mm512_load_ps(&B0[352 * kp_0 + jp_0]);;

					vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_0);
					mem_vec_0 = vec_0;

					vec_4 = _mm512_load_ps(&B0[352 * kp_0 + jp_0 + 16]);;

					vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
					mem_vec_1 = vec_3;
					scal_1 = A0[128 + kp_0];
					vec_6 = _mm512_set1_ps(scal_1);


					vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_2);
					mem_vec_2 = vec_5;



					vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
					mem_vec_3 = vec_7;
					scal_2 = A0[256 + kp_0];
					vec_9 = _mm512_set1_ps(scal_2);


					vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
					mem_vec_4 = vec_8;



					vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
					mem_vec_5 = vec_10;
					scal_3 = A0[384 + kp_0];
					vec_12 = _mm512_set1_ps(scal_3);


					vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
					mem_vec_6 = vec_11;



					vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
					mem_vec_7 = vec_13;
					scal_4 = A0[512 + kp_0];
					vec_15 = _mm512_set1_ps(scal_4);


					vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
					mem_vec_8 = vec_14;



					vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
					mem_vec_9 = vec_16;
					scal_5 = A0[640 + kp_0];
					vec_18 = _mm512_set1_ps(scal_5);


					vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
					mem_vec_10 = vec_17;



					vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
					mem_vec_11 = vec_19;
				}
				_mm512_store_ps(&C[J * i + j], mem_vec_0);;
				_mm512_store_ps(&C[J * i + j + 16], mem_vec_1);;
				_mm512_store_ps(&C[J * (i + 1) + j], mem_vec_2);;
				_mm512_store_ps(&C[J * (i + 1) + j + 16], mem_vec_3);;
				_mm512_store_ps(&C[J * (i + 2) + j], mem_vec_4);;
				_mm512_store_ps(&C[J * (i + 2) + j + 16], mem_vec_5);;
				_mm512_store_ps(&C[J * (i + 3) + j], mem_vec_6);;
				_mm512_store_ps(&C[J * (i + 3) + j + 16], mem_vec_7);;
				_mm512_store_ps(&C[J * (i + 4) + j], mem_vec_8);;
				_mm512_store_ps(&C[J * (i + 4) + j + 16], mem_vec_9);;
				_mm512_store_ps(&C[J * (i + 5) + j], mem_vec_10);;
				_mm512_store_ps(&C[J * (i + 5) + j + 16], mem_vec_11);;
			}
		}
	}
}
free(B0);
free(A0);

}
